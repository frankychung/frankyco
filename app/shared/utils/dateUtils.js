'use strict';

var months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

module.exports = {
  /**
   * Date comes in YYYY-MM-DD format. Return in January 7, 2015 format.
   *
   * @param {string} date
   * @return {string}
   */
  getDisplayDate: function(date) {
    var dateApart = date.split('-');
    var year = dateApart[0];
    var day = dateApart[2];
    day = day.toString().charAt(0) === '0' ? day[1] : day;
    var month = months[dateApart[1] - 1];
    return '' + month + ' ' + day + ', ' + year;
  }
};
