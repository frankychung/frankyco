'use strict';

var RSVP = require('rsvp');
var request = require('superagent');

/**
 * Taken straight from the example from:
 * https://www.npmjs.com/package/rsvp
 */
var getJSON = function(url) {
  var promise = new RSVP.Promise(function(resolve, reject){
    request.get(url).end(function(err, res) {
      if (err) {
        reject(err);
      } else {
        resolve(res.body);
      }
    });
  });

  return promise;
};

module.exports = getJSON;
