'use strict';

var { Dispatcher } = require('flux');
var assign = require('object-assign');

module.exports = assign(new Dispatcher(), {
  handleServerAction: function(action) {
    var payload = {
      source: 'SERVER_ACTION',
      action: action
    };
    this.dispatch(payload);
  },

  handleViewAction: function(action) {
    var payload = {
      source: 'VIEW_ACTION',
      action: action
    };
    this.dispatch(payload);
  }
});

