var keyMirror = require('keymirror');

module.exports = {
  ACTION_TYPES: keyMirror({
    RECEIVE_RAW_POSTS: null,
    RECEIVE_RAW_POST: null,
    LOAD_POST: null
  })
};
