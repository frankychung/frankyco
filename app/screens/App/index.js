'use strict';

var React = require('react');
var { RouteHandler } = require('react-router');

var Logo = require('components/Logo');

require('normalize-css/normalize.css');
require('./App.less');

var App = React.createClass({
  render: function() {
    return (
      <div className="app">
        <Logo />
        <RouteHandler />
      </div>
    );
  }
});

module.exports = App;
