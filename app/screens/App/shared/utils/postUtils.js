'use strict';

var assign = require('object-assign');
var dateUtils = require('utils/dateUtils');

module.exports = {
  convertRawPost: function(rawPost) {
    var dateApart = rawPost.date.split('-');
    return assign(rawPost, {
      year: dateApart[0],
      month: dateApart[1],
      displayDate: dateUtils.getDisplayDate(rawPost.date)
    });
  }
};
