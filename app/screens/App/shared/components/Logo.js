'use strict';

var React = require('react');
var { Link } = require('react-router');

require('./Logo.less');

var Logo = React.createClass({
  render: function() {
    return (
      <div className="logo">
        <Link to="posts" className="logo__letters">
          FC
        </Link>
      </div>
    );
  }
});

module.exports = Logo;
