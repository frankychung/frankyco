'use strict';

var appDispatcher = require('dispatchers/appDispatcher');
var { ACTION_TYPES } = require('constants/actionTypesConstants');

module.exports = {
  receiveRawPosts: function(rawPosts) {
    appDispatcher.handleServerAction({
      type: ACTION_TYPES.RECEIVE_RAW_POSTS,
      rawPosts: rawPosts
    });
  }
};
