'use strict';

var postUtils = require('utils/postUtils');

module.exports = {
  convertRawPosts: function(rawPosts) {
    return rawPosts.map(function(rawPost) {
      return postUtils.convertRawPost(rawPost);
    });
  }
};
