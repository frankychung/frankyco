'use strict';

var fetchUtils = require('utils/fetchUtils');
var postsActionCreators = require('../actions/postsActionCreators');

module.exports = {
  fetchPosts: function() {
    return fetchUtils('/posts.json').then(function(data) {
      postsActionCreators.receiveRawPosts(data);
    });
  }
};
