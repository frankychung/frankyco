'use strict';

var React = require('react');
var fetchPostsUtils = require('./utils/fetchPostsUtils');
var postsStore = require('./stores/postsStore');
var StoresMixin = require('mixins/StoresMixin');
var { Link } = require('react-router');

require('./Posts.less');

var Posts = React.createClass({
  mixins: [StoresMixin],

  stores: [postsStore],

  getStateFromStores: function() {
    return {
      posts: postsStore.getPosts(),
      lastViewed: postsStore.getLastViewed()
    };
  },

  statics: {
    willTransitionTo: function(transition) {
      postsStore.initialize();
      transition.wait(fetchPostsUtils.fetchPosts());
    }
  },

  componentDidMount: function() {
      document.title = 'Franky Chung';

      if (this.state.lastViewed) {
        setTimeout(function() {
          window.scrollTo(0, document.getElementById(this.state.lastViewed.slug).offsetTop);
        }.bind(this), 0);
      }
  },

  render: function() {
    var posts = this.state.posts.map(function(post) {
      return (
        <li key={post.slug} className="posts-row" id={post.slug}>
          <Link to="post"
            params={{year: post.year, month: post.month, slug: post.slug}}
            className="posts-row__title">
            {post.title}
          </Link>
          <p className="posts-row__preview">
            <span dangerouslySetInnerHTML={{__html: post.preview}} />
            {' '}
            <Link to="post" params={{year: post.year, month: post.month, slug: post.slug}}>
              ...more
            </Link>
          </p>
        </li>
      );
    });

    return (
      <ul className="posts">{posts}</ul>
    );
  }
});

module.exports = Posts;
