'use strict';

var assign = require('object-assign');
var Store = require('utils/Store');
var postsUtils = require('../utils/postsUtils');
var { ACTION_TYPES } = require('constants/actionTypesConstants');

var _state = {
  posts: [],
  lastViewed: null
};

var actions = {};

actions[ACTION_TYPES.RECEIVE_RAW_POSTS] = function(action) {
  _state.posts = postsUtils.convertRawPosts(action.rawPosts);
};

actions[ACTION_TYPES.LOAD_POST] = function(action) {
  _state.lastViewed = action.post;
};

var postsStore = new Store('CHANGE_POSTS', actions);

assign(postsStore, {
  initialize: function() {

  },

  getPosts: function() {
    return _state.posts;
  },

  getLastViewed: function() {
    return _state.lastViewed;
  }
});

module.exports = postsStore;
