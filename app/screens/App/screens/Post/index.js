'use strict';

var React = require('react');
var { State, Link } = require('react-router');
var fetchPostUtils = require('./utils/fetchPostUtils');
var StoresMixin = require('mixins/StoresMixin');
var postStore = require('./stores/postStore');
var postActionCreators = require('./actions/postActionCreators');

require('./Post.less');

var Post = React.createClass({
  mixins: [State, StoresMixin],

  stores: [postStore],

  getStateFromStores: function() {
    return {
      post: postStore.getPost()
    };
  },

  statics: {
    willTransitionTo: function(transition, params) {
      postStore.initialize();
      var { year, month, slug } = params;
      transition.wait(fetchPostUtils.fetchPost(year, month, slug));
    }
  },

  componentDidMount: function() {
    this._updateTitle();

    postActionCreators.loadPost(this.state.post);
  },

  componentDidUpdate: function() {
    this._updateTitle();
  },

  _updateTitle: function() {
    if (this.state.post.title) {
      document.title = 'Franky Chung: ' + this.state.post.title;
    } else {
      document.title = 'Franky Chung';
    }
  },

  render: function() {
    var post = this.state.post;
    var html = {__html: post.__content};

    return (
      <div className="post">
        <h1 className="post__title">{post.title}</h1>
        <div className="post__date">
          {post.displayDate}
        </div>
        <div dangerouslySetInnerHTML={html}>
        </div>

        <div className="post__back">
          <Link to="posts" className="post__back-link">
            <span dangerouslySetInnerHTML={{__html: '&larr;'}} />
            {' Back'}
          </Link>
        </div>
      </div>
    );
  }
});

module.exports = Post;
