'use strict';

var appDispatcher = require('dispatchers/appDispatcher');
var { ACTION_TYPES } = require('constants/actionTypesConstants');

module.exports = {
  receiveRawPost: function(rawPost) {
    appDispatcher.handleServerAction({
      type: ACTION_TYPES.RECEIVE_RAW_POST,
      rawPost: rawPost
    });
  },

  loadPost: function(post) {
    appDispatcher.handleServerAction({
      type: ACTION_TYPES.LOAD_POST,
      post: post
    });
  }
};
