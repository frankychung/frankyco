'use strict';

var assign = require('object-assign');
var Store = require('utils/Store');
var postUtils = require('utils/postUtils');
var { ACTION_TYPES } = require('constants/actionTypesConstants');

var _state = {
  post: {}
};

var actions = {};

actions[ACTION_TYPES.RECEIVE_RAW_POST] = function(action) {
  _state.post = postUtils.convertRawPost(action.rawPost);
};

var postStore = new Store('CHANGE_POST', actions);

assign(postStore, {
  initialize: function() {

  },

  getPost: function() {
    return _state.post;
  }
});

module.exports = postStore;
