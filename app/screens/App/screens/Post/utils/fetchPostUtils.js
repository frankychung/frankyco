'use strict';

var fetchUtils = require('utils/fetchUtils');
var postActionCreators = require('../actions/postActionCreators');

module.exports = {
  fetchPost: function(year, month, slug) {
    return fetchUtils('/posts/' + year + '-' + month + '-' + slug + '.json').then(function(data) {
      postActionCreators.receiveRawPost(data);
    });
  }
};
