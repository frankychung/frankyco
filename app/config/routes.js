var React = require('react');
var { Route, DefaultRoute } = require('react-router');

var App = require('../screens/App/index');
var Posts = require('../screens/App/screens/Posts/index');
var Post = require('../screens/App/screens/Post/index');

var routes = (
  <Route handler={App}>
    <Route name="post" path="/:year/:month/:slug/" handler={Post} />
    <DefaultRoute name="posts" handler={Posts} />
  </Route>
);

module.exports = routes;
