'use strict';

var fs = require('fs');
var yaml = require('yaml-front-matter');
var sortBy = require('lodash-node/modern/collections/sortBy');
var omit = require('lodash-node/modern/objects/omit');
var marked = require('marked');

module.exports = function() {
  var postsData = [];
  var posts = fs.readdirSync('./posts');

  function buildList(postsData) {
    if (postsData.length !== posts.length) {
      return;
    }

    var postsSummary = postsData.map(function(data) {
      return omit(data, '__content');
    });

    var sortedPosts = sortBy(postsSummary, function(data) {
      return data.date;
    }).reverse();

    fs.writeFile('./build/posts.json', JSON.stringify(sortedPosts));
  }

  posts.forEach(function(filename) {
    if (filename.indexOf('DS_Store') > -1) { return; }

    fs.readFile('./posts/' + filename, {encoding: 'utf8'}, function(err, data) {
      if (err) { console.log('error reading: ' + err); }

      var initialFront = yaml.loadFront(data);
      var yearMonth = filename.split('-').splice(0, 2).join('-');
      var date = filename.split('-').splice(0, 3).join('-');
      var preview = initialFront.__content.substring(0, 200);
      preview = preview.substr(0, Math.min(preview.length, preview.lastIndexOf(" ")));
      var front = {
        title: initialFront.title,
        slug: initialFront.slug,
        preview: preview,
        date: date,
        __content: marked(initialFront.__content)
      };

      fs.writeFile('./build/posts/' + yearMonth + '-' + front.slug + '.json', JSON.stringify(front), function(err) {
        if (err) { console.log('error writing: ' + err); }
        postsData.push(front);
        buildList(postsData);
      });
    });
  });
}();
