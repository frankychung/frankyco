# Franky.co

## First time

```
npm install
bower install
node build-posts.js
```

## Dev

```
npm run dev-server
npm run start-dev
```

Go to http://localhost:8080. Includes hot-loading React components. 

## Build

```
npm run build
npm run build-index
```

This will output to /build. You could upload this to static site or...

## Static files

Run build, then go to /build folder

```
python -m SimpleHTTPServer 7890
casperjs scraper.js  
```

Will crawl and write all posts's html files and replace index.html.
Dandy!

Note: There's a favicon in app/ but you need to manually put it in build
folder. It's been a while but when you first clone this repo you need to
manually add a `build/posts` folder.
