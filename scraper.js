'use strict';

var fs = require('fs');
var casper = require('casper').create({verbose: true, logLevel: 'debug'});
var __utils__ = __utils__ || {};

// http://stackoverflow.com/questions/25359247/casperjs-bind-issue
casper.on( 'page.initialized', function(){
  this.evaluate(function(){
    var isFunction = function(o) {
      return typeof o === 'function';
    };

    var bind,
    slice = [].slice,
    proto = Function.prototype,
    featureMap;

    featureMap = {
      'function-bind': 'bind'
    };

    function has(feature) {
      var prop = featureMap[feature];
      return isFunction(proto[prop]);
    }

    // check for missing features
    if (!has('function-bind')) {
      // adapted from Mozilla Developer Network example at
      // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Function/bind
      bind = function bind(obj) {
        var args = slice.call(arguments, 1),
        self = this,
        nop = function() {
        },
        bound = function() {
          return self.apply(this instanceof nop ? this : (obj || {}), args.concat(slice.call(arguments)));
        };
        nop.prototype = this.prototype || {}; // Firefox cries sometimes if prototype is undefined
        bound.prototype = new nop();
        return bound;
      };
      proto.bind = bind;
    }
  });
});

var helper = {
  baseUrl: 'http://localhost:7890',
  output: './build/',
  cleanUrl: function(url) {
    var lastChar = url.charAt(url.length - 1);
    if (lastChar === '/') {
      url = url.substring(0, url.length - 1);
    }
    return url;
  },

  findPath: function(url) {
    // gets last 3 segments
    var path = url.split('/');
    path = path.splice(path.length - 3, 3);
    path = path.join('/');
    return path;
  },

  findPathNoDate: function(url) {
    var path = url.split('/');
    path = path.splice(path.length - 1, 1);
    path = path.join('/');
    return path;
  },

  writeIndexFile: function(casper, prefix, noDate) {
    var self = casper;
    var url = this.cleanUrl(self.getCurrentUrl());
    var path = '';
    if (noDate) {
      path = this.findPathNoDate(url);
    } else {
      path = this.findPath(url);
    }
    if (prefix) {
      path = prefix + path;
    }
    self.echo(path);
    if (path.indexOf('http') !== -1) {
      self.echo('dont want to index top level page yet');
    } else {
      fs.write(this.output + path + '/index.html', self.getHTML());
      self.echo('Wrote file in ' + path + '.html');
    }
  }
};

var articleLinks = [];

casper.start(helper.baseUrl);

casper.then(function() {

  articleLinks = casper.evaluate(function() {
    return [].map.call(__utils__.findAll('.posts-row__title'), function(node) {
      return node.getAttribute('href');
    });
  });
  this.echo('Built array of articles links:');
  this.echo(articleLinks);

  casper.each(articleLinks, function(self, link) {
    self.then(function() {
      this.click('a[href="' + link + '"]');
    });
    self.then(function() {
      this.wait(200);
      helper.writeIndexFile(self);
      try {
        this.click('.post__back-link');
      } catch(e) {
        console.log('hmmm', e);
        this.wait(1000);
        this.click('.post__back-link');
      }
    });
  });
});

casper.then(function() {
  this.echo('Wrote index file!');
  fs.write(helper.output + '/index.html', this.getHTML());
});

casper.run();

