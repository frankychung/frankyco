---
layout: article
title: The worst feeling in the world, part two
slug: the-worst-feeling-in-the-world-part-two
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life notebooks
category: article
---
I've known for a while that it's important to keep something on you to
write on at all times, like a small notebook or an app on your phone
that you can use to jot anything at a moment's notice. It's to capture
those thoughts and ideas that are only there for a minute, before they
evaporate. But even knowing this, I still ocassionally get the notion
that I can remember it and that I don't need to record it. Maybe because
I think it's an especially huge thing, or maybe because I'll be at my
computer in five minutes. And half the time it's true, I can still
remember. But the other half, well that's the worst feeling in the
world.

When I hit these walls, I can never resist trying to get it back. The
thought that's definitely there somewhere, but it won't come out when
you call for it. It decides on its own when it wants to show itself.
It's like there's a part of your brain that has a life of its own. It's
frantically searching for something that isn't there. It's a hopeless
feeling, because once it's gone, it almost never comes back, at least at
that moment.

The only thing to do? Let it go. Don't worry about it. Besides, how many
great things are you already working on, and need to work on, thanks to
the times where you did jot things down? Things in the moment always
seem the most urgent but it's when you take a step back that you realize
what's really important. Besides, it's not like the thought is lost
forever. I've found that if you resign yourself to not being able to
rerieve it, it will find its way back to you, whether you remember it
was that thing you forgot or not.

And you know what's the crazy thing? It's an incredible relief to know
this. That if you don't stress it, your brain can and will do wonders
for you. That it's on your side. Just let it do it's thing. So suddenly,
this worst feeling in the world, it always segues into one of the best
feelings in the world, the one of letting go and knowing it's okay.


