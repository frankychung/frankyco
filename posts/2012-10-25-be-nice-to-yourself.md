---
layout: article
title: Be nice to yourself
slug: be-nice-to-yourself
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life
category: article
---

I had a hilarious thought the other day, about how I'm the only person
I'm forced to hang around all day, every day. The only break I have from
myself is when I'm sleeping, and even that's debatable. Is there anyone
that I can stand 24/7? I need my time alone, even for only a very little
time, to recharge how much I like someone. Which is very, very fast for
my very good friends, and pretty good for everyone else, I think. And
since I'm the only person I'm forced to be be around all the time, why
don't I try to be that someone who I wouldn't mind being around all the
time?

This brings up two things. One, I need to see myself as another person.
When I do that, I can view myself just like I view everyone else.
I've seen people be mean to other people. I've seen people be nice
to other people. I dislike or like them accordingly. I've also seen
people be nice or mean to me. I can gauge pretty well at this point
how people react to things people do or say. But I think we deceive
ourselves on how people might react to ourselves. We always think we're
in the right, we're angels, and whatever conflicts come up are just a
misunderstanding, a misunderstanding mostly on their part.

But if I stepped out and saw myself say the things that I say sometimes,
I wouldn't like it. You know how when you're away from home for a while
and then you come back and you really missed your family and you're just
so nice to them? But it wasn't like that in high school. You'd say mean
things, not because you dislike them or anything, but just because you
got used to being around them.

But if you were watching yourself, and you could will that person you
were watching, you would tell them not to snap. Or to take it back and
apologize. It's like when you're watching TV, or a movie, and one of
the characters says something stupid or does something terrible, which
they have to do or else there wouldn't be anything interesting to watch,
and you want to shout at the screen and tell them to stop, or to turn
around. Well this is the only chance you have to do that. You can change
the script on the fly. What kind of person would you love to hang out
with, be with? What kind of person would you like to be? Haven't you
ever thought what it would be like to be one of those people on the
screen, at least for a little while? Well this is your chance.

So there's you, and there's the you that you're watching. They're the
same person, which makes it terribly confusing, but don't think about it
too much and you'll start to grasp it. For that second person, dissect
what you like in a person. And just as importantly, what you don't like,
what you'd rather stay away from. This requires investigation on your
part. You might even need to write it down. In a more traditional sense,
you might call these your values. As you watch yourself, and don't worry
about keeping a perfect track record, take a split seconds once in a
while few to nudge yourself in the direction you'd want yourself to go.

Someone you'd be proud to tell other's he's your friend. Someone you
talk fondly about. Someone who you don't know everything about, but
are proud to know more about than most others. What do you want to see
in someone you'd want to get to know better? For example, say you're
walking down the street. When someone walks by you, you can make a
million assumptions about her. By her posture, by her clothes, by her
facial expressions. And I'm pretty sure all of us make a snap judgement
like, hey this guy looks cool, or he looks confident, something like
that. So when you walk down the street, look at yourself too. It's
superficial, but there's some truth to it, like how [bad posture is for
terrible people](http://franky.co/2012/07/slouching-is-for-terrible-people).

The good stuff comes from the people you know. Who do you admire? Or
thinking of it the other way around, who do you not like being around?
I admire people who work hard. Who exercise and eat well. Who read
and write. Whose work is good. Someone who knows what he's talking
about, and someone who doesn't talk behind people's back. Someone who
is curious about what they don't know and asks questions. Someone who
leads. Someone who reads. Someone who encourages and praises. Someone
who takes responsibilty. Someone that you don't know everything about.

You need to keep a careful balance. You're not trying to be someone
else. You're still you. For example, you might admire a leader, but you
don't have to be one yourself if that doesn't suit you. But you have to
at least lead yourself. Be your own role model.

Don't think of this as a different you. That's the last thing any of
us want. Your friends, your family, love, and love that they hate,
everything about you that is you. So don't throw any of that away.
Instead, think of it as the *best possible version of you*.

