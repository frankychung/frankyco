---
layout: article
title: Well-oiled
slug: well-oiled
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: design work running
category: article
---
You would think that running as fast as you can is easy. Just... run as fast as you can. But I've recently discovered that it's much, much harder than jogging at an easy pace.

When you run as fast as you can, and you haven't practiced at being any good at it, you notice right away there are certain parts of your body that feel awkward when doing such a foreign motion. It feels like things are out of sync. And so even though you're trying to go as fast as you can, you're not going as fast as you thought you would for going as fast as you can. There are awkward points in your stride and it's slowing you down.

For example, when you run as fast as you can, it's harder to control your arms. Aren't they flailing around too much? Did you notice you need to hold your arms a little further out from your body than if you were just jogging? You might not have been able to pinpoint it, but you could feel it. And your legs. I couldn't understand why it felt so strange running fast compared to jogging. Like my feet were filled with lead and it felt like I could trip at any moment. I mustered all my strength to figure out what I could do differently, to instinctively change something to make everything feel more in sync, and miraculously my body started to push my knees higher and it felt much, much better. Like a much needed injection of oil to loosen up the gears.

I probably could have read how to run properly somewhere. And I likely will later, since I don't want to end up hurting myself. But it's also these little discoveries you make on your own that make the whole thing worthwhile. It's these little tweaks that come to you from actually doing it. Someone could try explaining it to you in words all the details it takes to run properply, or even walk properly, but there are so many little things going on, you can only ingrain it by actually doing it.

So you have all these little things you discover, or you read about, and you want to try it, but obviously you can only run for so long in a day. That's the fuel. To be eager to get out there the very next day so you can hurry up and try that new thing. To be disappointed when your time and energy is spent and you need to hurry up and recuperate to be able to go at it again as soon as possible.

And say you do go at it every day, and you improve by an amount you didn't even know was possible, and at the same time you realize there's so much more to go. At some point it isn't even a question of motivation or willpower anymore. It becomes automatic. It moves from something painful to something that feels good. Imagine being able to run without risk of injury or without getting extremely out of breath, but you still get that exhilirating feeling after a good workout.

The joy of little tweaks and adjustments applies to everything. Cooking. Weightlifting. Writing. Even reading. Did you realize you can read better? You can read faster, comprehend more, and even enjoy reading more than you already do.

Then there's design. Design is such a broad term and there are so many moving parts that are necessary to make a successful whole. You won't get it right away. It might take years. I'm still at the point where [I know the stuff I'm making isn't exactly what I want](http://franky.co/posts/114-the-most-important-possible-thing-you-can-do-is-do-a-lot-of-work), but I feel like I'm getting closer, and I'll know it when I see it. But over the years, there are things that get ingrained in me, from just doing it or from reading it or from seeing it. Things like how important and how difficult it is to choose an effective color pallette. How much the grid can affect the usability and aesthetic of something. How much a single typeface can change the entire mood of a product. How design is almost entirely typography. It's things that hit you and make you realize that you can get better that make it a joy to do whatever it is you do.

Find the spots that feel like they are holding you back. You don't have to know exactly what it is, but move towards them, stare it in the face and try to figure something out. Just like when I was trying to figure out what to do with my feet, do a big push and something might come to you. This is what makes you feel good at the end of the day.
