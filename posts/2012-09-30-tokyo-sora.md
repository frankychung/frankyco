---
layout: article
title: tokyo.sora
slug: tokyo-sora
image: k13619737.jpg
embed: ''
link_url: ''
footnotes: ''
tags: life movies yokokanno
category: article
---
I don't write or talk much about movies. Same with music. There are
already so many talented people writing about movies and I could read
them all day, and they do the films more justice than I could ever do.
But the other day, I watched a Japanese movie on a recommendation, a
minimalist film where almost nothing happens, and I can't stop thinking
about it. And since I can hardly find any information about it online
(in English), I thought I'd give it a shot.

I've been trying to think about why I can't stop thinking about it, and
I think it's because this was such a new experience with *nothing*. If
you take most movies, you're focused on a group of people and a group of
events. It's no different here, except these people hardly say anything,
and hardly anything happens to them. But when you take away things
needing to happen, you stop caring about what's going to happen next and
you become entranced by what's *happening*.

It's about six women in Tokyo and you're just watching them do what
they do every day. Their lives are mundane, uneventful, and lonely. The
interesting thing is, only one of them seems to be unhappy. The rest of
them are just going about their day, like us. And that's what makes this
movie connect. Our lives are boring too. But it's peppered with little
events that could have an impact down the road. This is how we all live.
And that's perfectly fine. It's also perfectly fine to be unhappy and
lonely, although that doesn't mean you can't try to do something about
it.

There's no doubt, this film will not sit well with most people. It's
slow and in many senses of the word, boring. But it's like sitting down
at work. Work can be boring. But it can be fulfilling at the same time,
if you get into the right mode. There's the mode where you're in the
zone and you're getting a lot done, and time passes quickly, but there's
another mode where you're thinking hard, time does not pass quickly, but
you still feel good, if a bit exhausted, at the end of the day. This
movie is like the second one.

The music is incredible. There's very little of it, but what's there is
beautiful and helps lift this film in key places where it seemed like
the characters were getting deeper into a rut. Yoko Kanno is brilliant.

Tokyo itself feels like a character in the movie. Cold, cramped
apartments, bland offices, convenience stores, tiny bars, strange
nightlife, empty cafes, all staples of a big city, are pictured
beautifully here, accentuating the bigness of everything and the
anonymity of each of us.

I'm being misleading when I say nothing happens. Of course things
happen, but they're mostly disappointments, big and small. But the
wonderful thing about this is, it makes those small rays of light, that
small hope that things aren't so bad after all, all the more beautiful.

There's a difference between those quiet feelings of peace to those of
elation and joy. When something unbelievably great happens, it never
turns out as good as it seemed. That's something I've seen time and time
again. I mean, it happens some times, but most times, when something
truly good happens, you can see it growing and coming at you from miles
away. From something you've been working hard at, something you've been
cultivating in yourself. Even if it comes as a bit of a surprise, it's
been under your nose the whole time. These are the moments worth living
for.
