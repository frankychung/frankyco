---
layout: article
title: Paper
slug: paper
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: notebooks writing ipad design
category: article
---

It's tempting to think about carrying around an iPad, a good enough
stylus, and a sketching app that could replace your sketchbook with
almost an infinite number of pages. After trying it, although I'm not
100% sure yet, I don't think it'll ever beat a real notebook.

It's the difference between running and running on ice. Or like running
on sand at the beach. When you dig that pen into that paper, there's
subtlety in the tactile feedback that you don't realize you'll miss
until you try to do the same on a tablet. Say you're on page 32. When
you press down and the ink starts flowing, you're also feeling those
previous 16 pieces of paper underneath, and maybe the cover, ever so
slightly. On an iPad, you're hitting glass. There's an element of
*distance* from the tip of the pen to the ink coming out of it. (To
be fair, I haven't tried a retina display yet.) That distance is a
miniscule loss in control, but it makes all the difference. Then there's
the almost inperceptible lag. Even a fraction of a millisecond has the
similar effect.

I use sketches to spit out interface ideas. But so much about designing
an interface is organizing information, so with that comes a lot of
writing. That loss of control, however slight, is glaring when it comes
to writing. You can draw boxes and shapes fine, but the writing and
arrows everywhere are easier on the real thing.

I use a notebook every day. Not a journal really, but I use it to
organize my thoughts on whatever I'm working on. Maybe a plan, maybe a
map kind of thing, but writing words is the joy of having a notebook.
Even with a 7, 8 inch notebook, you can write a lot on one page. With
an iPad, you can write, but it's got to be bigger and it'll always be
clumsier to get it down.

There's also the sense of boundaries and confinement. With software, you
could theoretically have an unlimited canvas. But it still feels like
the boundaries are your screen. A sketchbook *feels* like it has more
room to breathe. It's because you can see and feel the empty pages and
the filled pages before it.

But even saying all of that... I love [Paper by fiftythree](http://www.fiftythree.com/paper).

