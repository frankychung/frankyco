---
layout: article
title: Temptations
slug: temptations
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: health facebook work
category: article
---
The other day I ran out of food in my fridge. I wasn't feeling up for the thirty minute trip to the store so I ate what was laying around. It was horrible.

When you're hungry, and you're feeling lazy, it's so easy to eat those chips, drink that soda, eat that candy, eat that bread. I love, love bread, but I know it's bad for you. You eat these things, and they taste amazing, but you feel like crap after. You may not know that you feel like crap, which I've seen in my friends when they look so proud of themselves after a bowl of ramen. Think about it. Your head gets a little sluggish. Your stomach feels heavy. You don't have as much energy to talk, or do, or make. 

On this day, I got almost nothing done, because I ate crap all day. Isn't that amazing? Two unrelated things suddenly have everything to do with each other. I've been to those all night hackathons where there was nothing but cola and chips and pizza. That doesn't keep people going. The adrenaline and time limit help you stay awake, but it isn't sustainable.

I went to the store the next day and bought three days worth of meat and vegetables. Because everything good was there for me to reach for, I never had to think twice about it. These next three days were very productive, especially compared to the horrible day preceding it.

We have a say in what we are with what we surround ourselves with. Food is just a small part of it. I can think of two more right away. Facebook. Don't get me wrong, it's great because it's the best way to keep up with and contact most of my friends. But there are also times when you spend half an hour scrolling down your news feed. It feels like eating a cheeseburger. You can't stop once you start, and it's kind of fun, but it sucks after. So figure out a way to only reach for it deliberately. (The same goes for Twitter, and maybe your RSS feeds, or Reddit, or whatever.) 

The second is television. I have a friend who doesn't even have a TV. He doesn't have to think about it at all. It's not an option, but that's a deliberate choice. There are plenty of other things lying around to grab his attention. He makes those things lying around count. 

And I have the perfect thing for you to have lying around. Books. Books have changed my life. They're the meat and vegetables for your brain. Try not to read too many five hundred word articles on the latest tech news and gossip. And if you're going to, find just the few writers whose opinions you trust and find interesting, and find the long form reads from great magazines. But also read books. I'm not saying have them physically lying around your room (although that'd be nice too). I hardly have any paperbacks because I buy books for my Kindle app. Our lives live in the screen, so the things you have lying around in your computer are important too. If you have an easy way to get to Facebook and Hulu, you're going to find yourself going there a lot. Instead, have an impossily long list of books you want to read, have a shortcut to where you can get to these books, and have your Kindle or whatever laying around so whenever you look at it you can think of reading the next chapter.

Find the other things that fascinate you that don't make you feel like shit after. Leave your guitar or piano in a place that will make you want to sit down and play when you walk past it. A good movie is one of the most satisfying things in the world. Even some video games make me so happy (I would say most multiplayer shoot-em-ups do not fit what I'm talking about). Find a way to constantly remind you of your unfinished projects. Find art and surround yourself with it. You'll be a healthier person.
