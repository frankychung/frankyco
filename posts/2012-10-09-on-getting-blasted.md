---
layout: article
title: On getting blasted
slug: on-getting-blasted
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life respect
category: article
---

I read a great, and surprisingly hilarious, book about Apple, although
it was mostly about Steve Jobs, called [Insanely Simple](http://www.amazon.com/Insanely-Simple-Obsession-Drives-Success/dp/1591844835).
There was a part
where they talked about how people got hit by the Steve Jobs
turret in meetings. Meaning, someone says something stupid, or
does something wrong, whatever wrong means to Jobs, and Jobs will just
blast the person.

The interesting thing is, when you got yelled at by Steve Jobs, if
he also didn't fire you, you walked out of the room actually feeling
*good*. Like okay, I have something to prove, something to take back,
and maybe in the midst of all that, I got a hint about something, about
a part of me or something that I hadn't seen, and since I wasn't fired,
now's my chance. It happened to me too, a few years ago in college.

One of our finance professors is one of my favorite people ever. He was
already past retirement age, but he would come in with such energy, and
he had such a charming southern drawl, and he always made it a point
to invite us over to his house for dinner, and at these dinners he'd
always find time to talk to each student, ask them how everything's
going, where they're from, you know, just to get to know them a little
bit better. He'd even invite the class to his favorite fancy restaurant
downtown, although I never personally took him up on the offer, mainly
because although I liked the guy, and although I was on a finance track,
I really couldn't care less about finance, so I was one of the quiet
kids that sat there nervously each class because he loved to cold call.

Anyway, this guy was smart and so passionate about making sure we were
learning, which I found to be a quality in many of my favorite teachers,
and also one of the key traits to get you to actually look up to these
people, to get you to talk fondly about them with your friends. So at
one point we were assigned a huge project, and we gathered in a team
that was a mish mash of concentrations, and it so happened that I needed
to be the lead finance guy, which I wasn't psyched about but I was okay
with, since I pretty much got the stuff, and I was on a finance track
after all. Intermittedly throughout the project we needed to stop in for
office hours to give a status update.

So maybe it was our second or third visit, and keep in mind I was the
guy who never raised his hand but could answer on cold calls so I'm
probably not his favorite kid, and he's flipping through our report and
asks a question. All heads turn to me and I fumble. And you know how
when you fumble, things just get worse and worse, until you're sitting
there with an empty tank in your head? Yeah. But at this point, he
wasn't upset yet, maybe he saw the color in my face draining, so he was
like, okay, and asked me a more elementary question, presumably to help
me get on my feet. Well, I didn't know the answer to this elementary
question, the question that any person studying finance for more than a
semester should have known by heart, although I knew I should have, so I
fumbled on that one too. And then he blew up at me.

I've been blown up at before. I can't remember one before this where
I just took it like I did. I would always fight back, knowing I was
one hundred percent right (and they always knew they were one hundred
percent right too, which is fair), and I would be pissed for days, maybe
even weeks. Those never end well. But this time, it was so strange,
because I took it, and I took it just fine. I admired this guy so much,
because he knew what he was talking about, and he actually cared, at
least I think he did, and I was absorbing every word he was saying in
slow motion and not feeling bad about it.

I think it's because in bad arguments, at some point, it gets to where
both sides are just trying to make the other person feel as bad as
possible. But this guy, even though he was upset, everything he was
saying was true and harsh. He was the kind of guy who was comfortable
saying what was on his mind, and although this was personal, it was
nothing personal, he was just laying it on the table as he saw it.

When someone like that is is frank with you, they say things that even
you were lying to yourself about. When you feel someone is being one
hundred percent honest, and they're not being a moron, there are no hard
feelings. In fact, I think I liked the guy even more after that.

I walked out of that room with more resolve than I had felt in a long
time about two things. One, of course, was that I needed to live and
breathe finance more, the concentration that I had chosen, for myself
and for my team, and I did. But the other thing he helped me see that I
had never even considered before was that finance is not for me. I'll
never forget his *good* yelling, the one that grabbed me and shook me
and finally woke me up.

