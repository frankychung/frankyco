---
layout: article
title: Out of reach
slug: out-of-reach
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: movies love lists
category: article
---
This question is so hard: What is your favorite movie?

It's not that I don't have a favorite movie. It's that I can never remember. My brain always tells me, yes, you do have a favorite movie, you love it so much, but right now, I can't tell you which movie that is. Then I'll be brushing my teeth at night and it'll hit me. And I'll remember how much I love it, and why I love it, and I'll wish that I could tell the person who asked me twelve hours earlier so I could try to convince her to watch it because it's one of my favorite things in the world to share good movies.

I don't have a single top movie that rules over every other movie I've seen. But there are some that live together in the upper echolons of my favorites. Any one of them could do as my number one. But when that frustrating, but totally valid, question is presented to me, I can't remember any of them. It's like a fuzzy image, I know it's there, but no matter how hard I think, it's just barely out of reach. At this point I just say, damnit there are too many to choose, I know I have a favorite but I can't remember right now, let me get back to you. The interesting thing is, I get this kind of response in return whenever I turn the question around. Is it like this for everybody? Why do we suppress this important part of ourselves?

Maybe it's a good thing. That we don't have this one movie constantly in our mind, so that every time we watch another critically acclaimed movie, we don't have to compare it to anything, getting our hopes up that it might be the one to topple the best so far.

But. It says so much about you, your favorite movie. It's good to walk around with something like that. Something you're proud of. It's like your clothes. It's an expression of yourself, just like the music you listen to, or at least the music you're not embarrassed for others to know you listen to. And I think Ebert said this, but it was something like, don't marry someone who doesnt love the same movies you do, because it won't work out in the long run. That makes sense if I don't think too hard about it. I mean, that special person doesn't have to love everything you do in a 1:1 ratio, but it's got to be roughly the same. It's like how you know you're connecting with someone because [you laugh at the same weird shit](http://franky.co/posts/21-all-dating-compatability-tests-end-up-testing-for). I don't know if this applies to friends though, because I've got friends whose taste in most things I abhor. But it rings mostly true with my closest friends.

So there's probably an easy solution for this. Jot a list down somewhere. Our lives are governed by lists anyway, what more is another one? And it may not feel like it, but I'm pretty sure we've all seen hundreds of movies. If you get into the habit of writing down the things you've seen, you may appreciate the whole movie thing more. As your list grows, you might think, hey, I like movies a lot more than I thought I did. And it may spur you to seek out more, to chase that feeling you get after watching a great movie. You might even need a whole other list, for all the movies you want to see, because that's another thing that's hard to answer. What movie did you miss last year that you really wanted to see but didn't have time for? You can never remember, until the title pops up somewhere, maybe on TV, maybe on your friend's shelf, or maybe just while you're surfing the internet. And it's fascinating because the bigger these lists grow, the more you discover what kind of person you are.

Like so many people say, you're made up of everything you surround yourself with. Movies, music, books, people, places, clothes, food. Don't let all of it go to chance (just some).
