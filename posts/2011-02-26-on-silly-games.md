---
layout: article
title: On silly games
slug: on-silly-games
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: orisinal
category: article
---
I love silly games. I especially love silly games on my iPod touch. A good one that comes to mind is [Doodle Jump](http://en.wikipedia.org/wiki/Doodle_Jump). I enjoyed it for about a day. Actually, I think I had more fun sharing it with friends than I did actually playing it. I would show off my high score and then watch them play it obsessively until they topped me.

But Doodle Jump is nothing compared to the [king of all silly games](http://orisinal.com). Every one of his creations are simple, addicting, and beautiful. Those first two are pretty much prerequisites to enjoy these kinds of things at all but that last one is hard to find.

A couple years ago I played [this](http://www.ferryhalim.com/orisinal/g3/bells.htm), which actually has its similarities to Doodle Jump but I love it so much more. (Not to mention it came around first!) It's hard to imagine but I was actually moved by this online flash game. The dark snowy background, the delicate music, the faint white bunny jumping around. A rabbit is such a natural choice for this. Bouncing on bells hung up like a Christmas tree. 

I still remember sitting in the library at school when I would get the impulse to take a break and fire this game up. Before I knew it twenty minutes would pass. It was okay though, since I would get this calm over me that I didn't have before I got the urge to procrastinate.

I love his other games too. I think the first one I ever played was the one where you capture [bees in bubbles](http://www.ferryhalim.com/orisinal/g2/bubble.htm). That was in middle school if I remember correctly. The music was the best part.

There's another one where you [help dogs jump over apples](http://www.ferryhalim.com/orisinal/g3/dog.htm). So good. [Throwing umbrellas to help falling baby chickens](http://www.ferryhalim.com/orisinal/g2/chicken.htm). Shockingly fun. [A bear catching bunnies in his back pack so he's heavy enough to bungee jump](http://www.ferryhalim.com/orisinal/g2/bear.htm). How does he come up with this stuff?

The other day I went on the App store and saw [Winterbells](http://itunes.apple.com/us/app/winterbells/id404410628?mt=8) for sale by Ferry Halim. I was so happy. Please enjoy it.
