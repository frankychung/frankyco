---
layout: article
title: Can't go back
slug: can-t-go-back
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: apple macbookpro windows
category: article
---
Someone asked me the other day why I use a Mac. I said something stupid about how I could talk about that for hours, and unfortunately for me, he said he happened to have some time. Of course I drew a blank. I've never really had to think through why I enjoyed using a Mac so much. I just assumed my love was rooted in enough logic that the reasoning would come easily to me.

First of all, the thing itself is beautiful. I've had mine for just a bit over 2 years now and it's still going strong. When I close the lid and right before I slip it into my bag, I still take a moment to admire its form. That's not to say I don't want one of those super thin ones that everyone's carrying around these days.

Its greatest strength in a side-by-side comparison to its competition is [the screen](http://franky.co/posts/5-happy-birthday). The color is so much better than that washed out crap on everything else. But we already know it's great hardware. We paid a premium for it after all. What else about it? The operating system is a big draw. Because of a few choice software on OS X, I'm absolutely wed to the thing. For example, I'll never be able to stand a start menu when I can just [LaunchBar](http://www.obdev.at/products/launchbar/index.html) it up. I love Preview's ability to quickly open anything, plus it's a great PDF reader. And I can't imagine living without [TextExpander](http://smilesoftware.com/TextExpander/), [LittleSnapper](http://www.realmacsoftware.com/littlesnapper/), [1Password](https://agilebits.com/onepassword)...

I've had the pleasure of meeting many developers and designers in the past few months. I don't think I know a single Rails developer that uses a Windows machine. I'm sure they exist so I guess I just don't find myself in the same room as them. It's just so easy to set up a development environment on a Mac. I spend a ton of time in the Terminal app, even with the beautiful UI giving me so much love. I don't use half of Lion's new features because I'm so enamored with how much freedom just the simple tools give us.

Then there are the reasons that people who seem to dislike Apple think people use a Mac for. Like, snobby reasons about being a creative person doing creative things that are too highbrow for Windows. I can't say that isn't true for some but I find it hard to believe that those are the reasons I use it, even on some subconscious level that I'm not seeing or choosing to ignore. It's more like my livelihood depends on me sitting in front of a computer all day, and the core experience of this thing is tight, smooth, but more importantly, makes me feel at home, like all my tools are ready at my fingertips. I've only been using it for a few years so it's safe to say I got used to Windows too, but I never got over the feeling that I was fighting, pushing back against, _things_ that you simply don't have to think about on a Mac.

On the other hand, Windows Phone looks amazing. The iPhone totally kills it with its hardware and unbelievable app selection but still, I wouldn't mind ditching it for a couple weeks to try that gem of a UI Microsoft has got going there.
