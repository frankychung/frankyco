---
layout: article
title: LittleSnapper
slug: little-snaps
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: littlesnapper evernote
category: article
---
There are very few programs on my computer that I use today that I used regularly two years ago. I hold one of them dear to my heart and am still surprised how much use I get out it for being such a niche-y product.

It's called [LittleSnapper](http://www.realmacsoftware.com/littlesnapper/). It's for taking and organizing screenshots of websites. 

There are plenty of ways to do the same thing. The most obvious is to use [Evernote](http://evernote.com). I actually used Evernote for a few months a couple years ago. But really, I only used the thing to keep my receipts and to save cool websites. When I stumbled on LittleSnapper, the tighter focus of the program was enticing. Plus I got kind of lazy about receipts.

Granted, I don't use what are probably LittleSnapper's differentiating features. I never annotate images. I don't upload images to their image sharing service. I just save and tag. But, I actually go back and use these images. 

I love the web snap tool. It automatically detects DOM elements and can auto select the boundaries. If a site is coded well, and when you stumble on a site that is snap-worthy it almost always is, you can snap a site's header, logo, navigation, and footer quickly. I do it so much that that's how I tag and organize everything. I know that looking at a beautiful footer isn't as useful out of context but seeing a gallery of your favorite footers side by side has its uses. Plus, it saves the source URL, just in case you need to go back. 

Also, I'm pretty sure you can't snap the entire width and height of a web page unless you're using something like [Paparazzi!](http://derailer.org/paparazzi/). LittleSnapper handles the task nicely.

Actually, I have a slightly unusual use for the thing. I love [ffffound](http://fffound.com). So much that I save the best pictures I find on there. I use another little gem called iView on my iPod touch and iPad and import them into iPhoto, then into LittleSnapper. I apply different tags than I do with the websites I save, such as _photography_ and _physical_ and whatnot, but there are also plenty of tags that overlap, like _color_ and _typography_. It's quite wonderful. I use these pictures more than I do the snaps of websites.

There are so many amazing images on the Internet. I can use LittleSnapper to save a little bit of it just for myself. I started doing this after reading a single post on Jon Hicks' blog. It was on his presentation about [being a creative sponge](http://hicksdesign.co.uk/journal/be-a-creative-sponge). It changed my life. And now LittleSnapper has the honor of being only one of three icons that live in my menubar.
