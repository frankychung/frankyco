---
layout: article
title: Morning words
slug: morning-words
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: writing morningwords
category: article
---
It's been just over two years now since I discovered [750 Words](http://750words.com). I think I found it on Lifehacker, which I don't usually like because you read a lot about quick fixes to help you do more things faster but you never find anything that sticks. Take, for example, a new system to get things done. Nothing ever lasts long enough to stop me from going back to pencil and paper.

But there's something different about 750 Words that has had me hooked for two years. I can't think of anything else that has grabbed me for that long. And I don't see myself ever letting it go.

It's a simple premise. Every day, write 750 words, and upload your writing to the site for it to keep track of how many days in a row you do it. Well, I think it prefers you write the words on the site itself so it can keep track of how long it takes too, just for fun stats, but it doesn't have a problem with me copying and pasting from my preferred text editor (notice I didn't say word processor).

The service was inspired by the book The Artist's Way and one of its big takeaways, to write in the morning every day. More specifically, to write three pages, and to not think about it too much. Treat it like a brain dump and write it with a stream of consciousness. This is different than writing for work or pleasure, so you're not really supposed to go back and polish it up, or even read it again. It's an outlet for you to get things out of your head so you can focus on better things. And it works.

So why is it called 750 Words? Because typing 250 words is roughly the same amount of writing as hand writing one page. Obviously it depends on how big a page and how small your writing is but I wrote enough papers in school for that to make sense.

It felt like a lot to write at first. I struggled for a couple weeks to think of enough to write. And although I wrote it in the morning sometimes, I would usually procrastinate until the evenings, which doesn't make it as effective (more on that later). I skipped days too which hurts your chances of getting the hang of it before giving up.

But one day it clicked. Actually, I think two things clicked. The first was feeling like the floodgates had opened. It suddenly became easy to write (type) three pages without stopping, which halved the time it took for me to finish. The second was that I finally understood the big deal about writing these morning words at all.

It turns out there's a lot going on in your head. You hear it too, all the time, but you dont realize what it's doing to you. You're thinking and worrying and smiling and laughing about everything thats happening right now, about everything that's already happened, and about everything you think is going to happen. But all of this is bouncing around in your head with nowhere to go. Some of it manages to get out through certain channels, like the things you do or say, or it just sort of evaporates and you forget about it. But there's never any space. Sometimes the pressure builds up so high in there that you go a little crazy. Just a little. I think it's the root of irritation.

So this uninhibited writing, where you're writing to nobody in particular, where you can share, whine, and think about anything with no filter, is the ultimate vent for these voices. I was having difficulty filling up the page at first because watching my thoughts materialize into words in front of me was strange. I never had this kind of outlet before. Once you get used to that feeling, you suddenly become honest with yourself, and it's simply a matter of reaching into your head and skimming the top of an infinite pool of thoughts to fill up three pages.

It's a form of meditation. It clears your head, temporarily, so you can focus on your art, whatever it may be. Now, there's no time of the day that this will work exclusively, but it feels like the morning is better. And I'm talking about the morning relative to you, so if you wake up at five PM, more power to you. Just do it before your hard work for the day and you might find yourself thinking a little clearer, and feeling a little bit better about yourself, which might help even a little bit to clearing the path to doing your good work. The evening is okay too though. It's a great cap to a busy day. It's like tidying everything up and clearing the table after a good meal. It's like how it feels good to wash the dishes and take out the trash. You might sleep better with a clearer head, and you might wake up easier too. The important thing is to do it at all, every day.

It's training yourself to face a mini struggle everyday. I said it's not hard to find the material to fill the page once you get used to it, but the act of filling or typing three pages is still a bit exhausting. It's the good kind of exhausting, like when you have a good workout at the gym. I suspect it will take you twenty to thirty minutes at first, but eventually you'll be able to pare it down to ten to fifteen minutes. And you'll find some days it will be hard to spend even fifteen minutes to do it, because you're tired, or you're running late, or your favorite show is on. You don't realize that fifteen minutes is nothing. So if you can make something like this a habit no matter what comes up, no matter your mood, you can train yourself to fight those voices that are constantly trying to convince you to take the easy road.

You should exercise every day. You should wash the dishes and clean your room every day. You should floss every day. You have limited willpower to fight through all these things you should be doing. But morning words hits the sweet spot. It's hard to do, but not too hard, and the ratio of its benefits to its difficulty is so high that it's the perfect way to build your resistence to that voice that tells you to relax when its time to do anything worthwhile. You will watch yourself thinking about writing, telling yourself you should, and then not do it many, many times, just like exercise. But you will also watch yourself resist that voice and sit down and do it more and more as time goes on. And you will see this resistence build to other aspects of your life where you should be resisting.

I think 750 Words gets this too. So much of the site is built around keeping a streak going. On every new day, you see a big row of boxes at the top with Xs for all the previous days that you finished. And there are badges for the number of days in a row you keep it up. Oh those badges. They work incredibly well. I managed to get to something like 370 days in a row before a snafu caught me. At that point I figured I was getting too obsessed with the streak so it was okay to let it go and start over.

There's one more thing about morning words. It's helped me write more. I'm not saying it'll help you become a better writer, but you may fall in love with the act. You realize that writing permeates all aspects of your life, that putting words together in a beautiful way is art and feels good, that it's challenging to be clear and concise and helps you think and think well, that everyone has their own style and it feels good to have your own, that getting better at writing is a lifelong pursuit and it feels good to watch yourself improve. Now, dont worry about spelling or grammar or the style of your morning words, save that for everything else. Just starting your day with pen to paper or hands at keyboard will grow your love for writing bit by bit.

So say you start to enjoy writing, or you always have. Suddenly, you have something to look forward to, something to wake up for, *every single day*.
