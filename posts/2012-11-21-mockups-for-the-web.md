---
layout: article
title: Mockups
slug: mockups-for-the-web
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: design webdesign webdevelopment rails
category: article
---

Someone who designs for the web is someone who designs the thing that
you click around in on your screen. There's probably going to be some
typing as well, but that's about it. It's hard and rewarding work and
I'm lucky that it's what I get to do for most of my day. 

A crucial part of this work is the beginning stages of every product:
The Mockup. You take what's in your head, or in your sketchbook, or
your PSD, and write the HTML and CSS. But even with that, all you have
is a lifeless page. You still haven't dealt with the most basic thing:
clicking around. You're stuck in one place, and that's not how websites
work.

Your users want to go places, do things. A good interface is letting
someone go places and do things easily. So how do you mock that up? I
like Rails, but something like Sinatra might just be perfect for this.

I'm saying to make your mockup functional. Skip the database and have
your routers and controllers return fake data. You suddenly have so
much more to work with, to help you think about how your information is
organized, how difficult it is to get from one place to another and
how easy it is for your users to accomplish what you want them to get
done.

We who call ourselves web designers have a responsibility to learn to
go beyond HTML and CSS. The web is about *interactions*, not how things
look. If you can get beyond designing things that aren't connected to
each other through interactions, as you iterate your mockup, you're
iterating your soon-to-be-born product. It's a wonderful feeling.
