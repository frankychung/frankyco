---
layout: article
title: Can't imagine
slug: cant-imagine
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: music movies work
category: article
---

Think of a song you've been into lately. Think about those five minutes
that makes you feel something. Sometimes it's so good a part of you
wells up inside. Then you discover another of a million other amazing
songs and you move on.

Now think about the group of people who made that song possible. Your
favorite bands release a new album maybe once every couple years, if
you're lucky. What takes them so long? We take those five minutes for
granted.

What do you think about when you're laying in bed at night when you
can't sleep? What do you think your favorite musicians are thinking
about when they're laying in bed at night when they can't sleep?
They're thinking about that song. They're thinking about that song for
weeks. Maybe months. That one, single song. Something that is a blip in
everyone else's universe.

And that is amazing. I can't even imagine what it's like to think
about music so deeply for so much of your day. I don't know *what* to
think about when it comes to music. I see the end result but I can't
understand the effort. And you know what? Everything that's worth
experiencing comes from people putting hundreds of hours of effort per
five minutes of consuming it.

Take movies. 2-3 year production times are normal. If it takes a
musician a year to release an hour of music, it makes sense for a crew
of hundreds to take a couple years to release a big film. Everyone on
the staff goes to sleep thinking about this movie for *two years* that
you're going to watch in *two hours*. Every second, every word, every
movement, every camera angle, every chord, every instrument, every
costume, has come from someone's hard work and deep expertise.

Can you imagine trying to perfect a song for weeks at a time? Crafting
the perfect lyrics, tweaking the melody, adjusting the mix, and whatever
else they do. Do you think they grow tired of it? I'm sure it's
stressful and maybe even miserable sometimes, but I'm also starting to
think they like what they're making more than you or I ever will, even
if we're their most rabid fans.

I've spent the better part of the last two years building and growing
one thing. Same with the rest of our small team. We live and breathe
what we do. It's what I think about when I can't sleep. And in
that way, I can imagine the *feeling* of being a musician, a filmmaker,
a writer, an actor, a carpenter, a restaurant owner, etc. We make and do
things that some people don't understand but love. We build our skills
to a level that no one has any *idea* what's going on in our heads. We
spend our life to make a little part of someone else's better.

I can imagine it because if I take my two-year-younger self and have
him look at me now, I would be as dumbfounded with what's going on in
my head as I am with whatever went through the head of whoever made
the amazing song I'm listening to right now. And I think you can do
the same. What are you building? What do you do so when people look
at you hard at work, they can't even imagine where to begin trying to
understand what's going through your head? What do you do that makes
people smile, laugh, or cry?

