---
layout: article
title: Lock and key
slug: lock-and-key
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: 1password passwords
category: article
---
Let me guess, you have one password you use for pretty much everything. Maybe you have another one, for throwaway accounts, or another one you use only on special occasions. In any case, the number of passwords that you have in your head isn't the hard thing to keep track of, it's knowing which passwords go with which accounts. I've made at least fifty different accounts on various websites and apps over the past year, and that number is only growing faster (although Facebook is making things a bit easier to manage). I knew I was in trouble when two things happened.

The first is when I accidentally let someone know my one-password-for-everything. Now, there are maybe two or three people in the world I would trust this password to knowing they would never even consider misusing it. I don't really have a guideline of what's unacceptable but the mutual understanding is what makes us best friends right? Anyway, some time after that, this person logged in as me on another account of mine with the same password to play a harmless joke. This had never, ever happened to me before and it was a wake up call. I had no hard feelings for the guy, but still, I needed to address the alarm blaring in my head, the one telling me I needed to change my password for everything I use. So I did, and it was a pain, and I knew it would only take one slip up for me to have to do the whole process over again.

The second thing was when everything was getting too out of control to remember. At this point I had three passwords because of the accounts I was too lazy to change or didn't remember I had, plus a variation on some of them when I didn't have the right number of characters, or when I needed an alternate letter number combination. Basically, for all my accounts that I didn't access every day, I was consistently pressing the forgot password link, and it was getting tiresome.

So there is this program called [1Password](https://agilebits.com/onepassword). I tried it a year ago but I didn't get the appeal. You save all your passwords in the program and it remembers each URL and username and password which you can quickly copy to your clipboard and paste in the password field of whatever you're trying to log into (it can save other useful things like credit card numbers). The cool thing is it also generates long passwords that are impossible to guess so the idea is to have different passwords for each thing you use. You have one master password to reach the rest of your passwords.

I didn't get this the first time but the implication of this is that you _never have to remember a password again_. That is a huge relief. That and each account's password is buried under layers of security that makes it exponentially more secure. The first thing everyone says to me when I try to convince them to switch to a password manager is that once someone knows your master password, the whole thing is invalidated. Not exactly.

For one, they need to have your computer to get to any of the passwords. So immediately, you eliminate everyone online who could get to your stuff. Now say your computer is stolen. The punk who stole your computer probably isn't even going to know what a password manager is, much less guess your master password.

And say someone you don't know somehow finds your master password. That's all they know. They can't access your email, your bank account, anything. Just a password they can use only by prying your computer from you, at which point you've already changed your password if you knew it was compromised. So although things can still go wrong in the most rare, dire circumstances, the system is so many multitudes tighter than whatever you're doing now, and I love it.

The second thing everyone says is that they don't want to bother having a different password for everything. This is another thing I didn't realize until I started using it, but having a different password for everything is _faster_. How? Well, for one, you'll never be pressing that forgot password link again. But secondly, at least with this particular program, it comes with browser plugins that automatically fill in forms for you. You can also open the program and click the account and it opens it in your browser, fills in the fields and presses submit for you automatically.

What really sealed the deal for me was the iPhone and iPad apps. Actually, since I access many of the same accounts on all my devices, this was a deal breaker. Thankfully the apps are great. It's the only easy way I can think of to access your email or whatever on another computer besides your own.

I'm in love with this software. It's the perfect use of technology to solve this stupid problem everybody has in such a duh way. These people are making something so important that absolutely everyone can and should benefit from. How lucky would I be to be able to work on something like this? Don't they just pat themselves on the back every day for making something so great?
