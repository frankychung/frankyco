---
layout: article
title: A million bucks
slug: a-million-bucks
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work cleanliness life
category: article
---

Shopping is fun. When you have some extra cash to spend, it's fun going
out, and getting a new shirt, or maybe some new shoes, or maybe even a
gadget you were thinking about, without thinking about it too much.

That first day you have your new thing is important. You might get it
and hang it in your closet for a week or two, or maybe you're itching for
the next day to come so you can put it on, but that first time, it makes
you feel like a million bucks. It's like that new car smell. New
shirt smell is nice.

When you feel like a million bucks, something happens. You walk with
a skip in your step. The day feels like it's going to be a good one.
You take a glance when you pass your reflection from a store window on
the street. You might even smile more, and find it easier to say hi to
people in the morning. Confidence is gold and it's great you can get it
something you slip on before you head out the door.

And people notice. Not that that's so important, but you don't have to
ever think about people thinking otherwise. It's one less thing to worry
about. Now you have more time to think about more important things.
Like doing good work. Or taking care of yourself.

The question is, how do you feel like a million bucks every day? It's
not like you can wear a new shirt every day. 

*Stay clean*. I think one of the reasons that first day is awesome is
because that's the best that shirt is ever going to look. Every wash
dims it a little, each stain hurts. But you can be careful. Careful when
you wear it, careful when you wash it, careful when you hang it up. And
it isn't just your clothes. You ever wake up late and have to rush out
without a shower (or whatever your morning routine is)? Shave, comb your
hair, polish your shoes. Put on a watch, iron your pants. Looking clean
makes you *feel* clean.

Keep your house clean. It feels so good to walk into your room and not
have to pick anythhing up. To see your bed neatly made. To not have any
dishes in the sink. 

Exercise. When you hit the gym, or maybe go for a jog, you feel like
a different person from thirty minutes before. Maybe it's that slight
soreness that comes with a good workout, or maybe it's that boost
of energy, or maybe that knowledge that you did something good for
yourself. Think of exercise as a kind of cleaning. Just like you wouldn't
go days without brushing your teeth in fear of letting it rot, you got to
exercise to keep your body and health together.

Keep your work clean. Don't let your emails pile up. Keep your
desk organized. If you have to dig around to find things, and you're
constantly forgetting where things are, or are scarred things are going
to break and you can't do things with confidence, you're forced to
worry aout things other than solving the problem you're working on and
creating something awesome. If you can end the day knowing your work is
good, clean, and beautiful, you can walk out each day feeling like a
million bucks too.
