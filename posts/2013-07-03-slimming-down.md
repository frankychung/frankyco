---
layout: article
title: Slimming down
slug: slimming-down
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: writing programming design
category: article
---

It feels good to write a lot about something. When you've got something
to say, writing is one of the best ways to say it. But you know those
times when you ramble? You're talking to someone, and you're not making
much sense, and both of you know it, and you chuckle nervously and
change the subject. Or when someone is just completely bullshitting you,
and you can't wait for it to just end. If you're not careful, you're
going to write yourself into a situation like that.

But the harder something is to think about, to talk about, the easier
it is to slip into that. The bigger the point, the longer it takes to
get to it. Say you're writing an essay for school or something, and
you're juggling a million things in your head. Maybe you manage to get
all those million things out, and you're exhausted and you pat yourself
on the back, and you put it away for the day. You come back, and you
realize it's actually a fucking mess. You're only half done. You need to
take those million things and make it comprehensible.

Now, you could somehow be careful and get it right the first time, but
that sounds a hell of a lot harder. Instead, it's easier to finish,
walk away, and come back in a couple days and kill your precious words.
And even better, your precious ideas. Meaning, what if you had to cut
out half the *things* you wanted to say, to get across the important
point(s) you *really* wanted to say? Once you get used to that idea,
that there is a likelihood that the sentence you're writing right now is
not going to be there in the end, it becomes a lot easier to be *clear*.

You know when things get out of control, and something kicks in, you get
a shot of adrenaline, and you always manage to get yourself out of the
chaos? Some people love that shit. That might be the way you work best.
I think it is for me.

Take my work. If I'm juggling a million things, I'm probably not doing
it the best possible way. But sometimes, something is so huge, or so
new, or so difficult, that it's still *easier* to jump in, go crazy,
and figure my way out and drop things later. Maybe next time, if I do
something similar, or someone else on the team knows it better than me,
we can step around it more carefully, but what better way to learn than
digging yourself out of holes?

And then there are the people who use, or read, my work. Just like how
I want to say the simplest possible thing at any one time, I want to
make things that is the easiest possible thing to understand. And that
doesn't mean just changing and iterating it. It means chopping away at
it. Removing sentences and ideas from an essay is like removing features
from a product to make it stronger. Not necessarily so that it only says
one thing, but one *main* thing. That's writing, that's programming, and
that's design.

