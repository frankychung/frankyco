---
layout: article
title: Coincidence?
slug: coincidence
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: friendship work conversations movies music
category: article
---

There are people who I can talk to for hours about anything. Then there
are the people who I can't talk to at all, the ones where it's kind of
awkward. Most people are in between. Of course I could work on that, so
that there would never be any awkward conversations. But at this point,
the ones where the conversations are the best, those come naturally.

Among this group, I've discovered a peculiar thing. We almost always
have something in common, and it's that we love the same movies, or
music, or books, or maybe all three. Not that we necessarily talk
about it. Maybe I'll find out after the fact. But these things that we
love, they shape us, so much so that I think, do they shape our entire
personalities, our flow of thoughts in a conversation, our outlook on
life? I'm starting to think, *yes*.

But the even more wonderful thing I've learned about this group is how
there is the huge potential for the discovery of even more things you
might love that you never knew existed. Oh you love that band? Have you
heard of these guys? Or I'm reading this book right now, I bet you'll
love it. Or I've seen every one of his movies, and you've got to check
this one out. There's bound to be something there.

And on the flip side, you'll almost always have something to offer.
Doesn't it feel so good to [recommend something](/2012/10/recommendations)
and having the other person love it?

It's amazing how we meet the people we do. These things that feel so
personal, the things we simply call our favorite things, they play such
a huge part. I wonder, how does it feel to create one of these things?
To know that you made something that at least two people who've never
met each other really love. To pour a year, two years, five years into
something like that. Is that why people who make these things do it in
the first place? Doesn't it make you want to make something yourself?
