---
layout: article
title: Please read books
slug: please-read-books
image: ''
embed: ''
link_url: ''
footnotes: ! "1. <span id=\"fn120110413\">Well I don't know, I don't actually go to
  the library much. I'm just assuming the shelves are full of old romance novels.</span><a
  href=\"#fnr120110413\" class=\"fn-return\">&#8617;</a>\r\n2. <span id=\"fn220110413\">I
  recommend [On Writing](http://en.wikipedia.org/wiki/On_Writing) by Stephen King
  and [Bird by Bird](http://www.amazon.com/Bird-Some-Instructions-Writing-Life/dp/0385480016)
  by Anne Lamott. They're good reads even if you're not interested in improving your
  writing.</span><a href=\"#fnr220110413\" class=\"fn-return\">&#8617;</a>"
tags: books
category: article
---
I don't know one person that wouldn't benefit from reading more. One book can change your life. The crazy thing is, I keep stumbling on these single life-changing books over and over. 

It's not so easy to walk down the aisle of Barnes & Noble and pick out a winner. I know I never did that. I think it might be even harder at the library.<sup id="fnr120110413">[1](#fn120110413)</sup> Instead, wait for the name of those books to come to you. 

Allow me to explain. The books that are right for you will probably be read by someone you already know and trust. Not just your friends and family, although those are some of your best sources, but also the people you read, the people you follow, the people whose work in your field you admire and respect. 

I took a short study abroad course in London when I was a rising senior in college. It was a blast, but also, surprisingly enough, educational because we would spend each afternoon visiting a bank or a hedge fund or whatever (I was on a finance track back then). We walked into the office of Ziff Brothers and met one of the head guys there. The moment he walked into the room we all subconsciously straightened our backs and perked our ears. Just his gaze was intimidating. He immediately started to dump so much knowledge and wisdom onto us we could hardly breathe. It was awesome.

Q&A was almost over when someone asked an intriguing question. Paraphrased, it was something like, with the time remaining we have in college, what do you recommend we do to become even a fraction of the awesome that you are?

Without a moment of hesitation he gave only one piece of advice. Read. He was the one that told me that even one book can change your life. The one book he recommended I immediately tracked down and devoured in two days. (It's called [The Art of Learning](http://www.amazon.com/Art-Learning-Journey-Pursuit-Excellence/dp/0743277457), if you're curious.)

It's been a slippery slope ever since. I love reading books about productivity. About time management. About creativity and learning. I even read a book called How to Read A Book which was fascinating, even though everyone snickers when they see it in my bookshelf. 

The great thing is that all these people say similar things in different ways so you start to see the big picture as each story teller reinforces it.

I've become a different (and I hope better) person as a direct result of the books I've read the past few years. For example, I started reading about Vipassana meditation and now set aside a chunk of time _every day_ to practice it. It's become a permanent part of me. 

Books solve mysteries that have stumped you your whole life. Take learning how to write. There are so many good writers out there. They certainly didn't just pick up a pen and instantly start writing masterpieces. Learn from the ones that are willing to share what they know.<sup id="fnr220110413">[2](#fn220110413)</sup>

Now I'm not only talking about personal development books, although I'm quite a fan of those. Read anything and everything. Read it for enjoyment but savor it for how well it's written and imagine the skill, effort, experience, personality, and research it required to write that book. Have your own opinion on it. Discuss it. No matter what, even if you read a stinker, you'll come out better for it.

In every kind of fiction, the writer is writing about some truth they know about. Don't think about it too hard but it's fun to keep in mind where the author is coming from, where the characters are coming from, and where you stand in between. Who are you rooting for? How are you imagining the scene in your head? Why is it exactly that you can't put the book down? The feeling you get after a good read is just like after a good session at the gym. That's a heck of a lot better than zoning out with the Food Network (which can be good too, sometimes).

I'd go as far as to say you can tell a lot about a person from what they read, or what they recommend you read. Or if they read at all. Not to knock those who don't fancy a good book but like I said, they're missing the opportunity to become better from it.
