---
layout: article
title: Addictions
slug: addictions
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life reading writing
category: article
---
It's hard to escape addictions. I know it's possible to move on from a single addiction, but I don't know if it's possible to escape being addicted to *something*. I'm sure there are an amazing few who have pulled it off, but for most of us there might be a better solution to rid ourselves of bad addictions. 

I've been addicted to a number of things. Counter-strike, soda, cigarettes, coffee. Actually, I'm still on some of those. And there are the less obvious things like being addicted to checking Facebook or my RSS feeds religiously every day. But there were things I was addicted to I wish didn't drop. I used to be addicted to exercising every day, whether it be going to the gym or running around the neighborhood. When I was a kid, I got a high out of playing tennis. When I was even smaller, I used to love practicing the violin. A little later I would play the guitar every day, but not any more these days. I call these addictions because I would feel bad if I didn't do them.  

So there are obvious bad addictions that you don't want to get into, or you want to get out of as fast as possible. Things like drugs and alcohol. I don't need to list those out for you. But think about the good addictions. What if you were addicted to books? Or addicted to working on that pet project of yours? Or addicted to writing every day? Something that would make you, if you didn't do by the end of the day, or if you somehow forgot, wake up in a sweat because you didn't do it. 

Addictions can control your life. You will go out of the way to satisfy your addiction because you need it, even if it's just a quick hit. All of us have little addictions, and most of ours are harmless. Like that TV show you watch every week. Or that snack you sneak every day. I don't think you have to worry about the harmless ones, although harmless ones can quickly turn into bad ones when you increase the volume. And although it's possible to drop an addiction that's hurting you, it's easier to *displace* it with another one. Why not purposely find a good addiction?

A good gauge for a good addiction is its output. There are addictions that produce something. Ones that require you to act. Think playing an instrument, or writing, or playing a sport, or any sort of exercise. Compare that to just consuming. Watching TV all day. Or surfing the internet for hours. 

But not all consuming is bad. Do you think? Are you stimulated? I take that to mean reading books. Or visiting museums, or traveling. Or actively seeking out good movies to watch. I spend a lot of time finding and watching movies that make me think, make me sigh afterwards knowing I just watched something great. Do you feel good tired, or tired tired after it? The kind of tired is the best gauge I've come up with for good consumption addictions. 

Here's my point. There are many things that will threaten to consume you. It's hard to fight it, so don't. Pick the best ones to let in and don't give enough room for the others.


