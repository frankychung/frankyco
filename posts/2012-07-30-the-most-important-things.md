---
layout: article
title: The most important things
slug: the-most-important-things
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work macbook retina
category: article
---

Let's talk about the most important things you can buy. There are plenty
more important things you can't buy but we think about that all day
already. I've found I and many others don't think about some of the
things we can card. And even then, you should probably be saving up for
that trip with your friends and family instead of some new gadget or car
or whatever, but right now I want to talk about shit you can walk into a
store and walk out with (or order on Amazon, whatever).

I've always heard that you need to spend as much as you can afford on
a good bed and nice shoes. You're either in one or the other. (Please
don't tell me you sleep in your shoes, you weirdo.) So I did just that,
and I'm so glad I heard it so early in life to be able to get a good
night's sleep every day and to have cured my foot and knee pains for
hopefully forever. If I ever lose one of these two things, more than
anything else, I'll try to get another good one as soon as possible.

I recently upgraded my Macbook Pro to the new one with a Retina display
(it was a gift, and an amazing one at that). I suspected it would have
been worth the upgrade. I've been using it for a week now at the time of
writing this, and it's incredible. Just like you've heard. But just like
that movie everyone's raving about, you don't get it until you actually
watch it. It moves you in your own way.

I think I have a better way to describe it than just incredible: Just
like you owe it to yourself to spend as much as you can afford on a
good bed and good shoes, if you spend the majority of time staring
at a screen like so many of us do, you owe it to yourself to get a
retina screen. Maybe you already have the iPhone that's been out for two
years, or the iPad with retina, but this is a quantum leap above that.
Especially if you code, there are so many day to day things you do only
on your computer that are rendered into an absolute wonder to look at.

In fact, if you code at all, you're in for the biggest treat out of
everyone. I'm using Inconsolata at 15pt and the shock still hasn't worn
off. The text on my screen looks almost like paper. Reading on a retina
screen is a pleasure, but writing and creating and building on one is a
revelation.

It's about the tools you use every day. Not just the tools you work
with, but the tools you use to get around, the tools you use to rest,
to cook. The important part is that you use it every day. This thing I
took for granted, this world I live in that is my screen, was turned on
its head and it took that much to get me to realize this. Now I'm taking
a good look at some of the other things around me that people have been
saying for ages is important too. My keyboard. My chair. Things that
might be counterintuitive to not skimp on.

Of course surround with yourself with the best people you can. See and
experience as many things and treasure those. Do good work so that you
feel great at the end of the day. But think about the easy wins to
improve the quality of life. Not in terms of price, but in the one thing
you can change to make something you do a lot significantly better,
easier, and or simpler.
