---
layout: article
title: Improvise
slug: improvise
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: productivity habits
category: article
---
I try hard to plan ahead. It's even kind of addicting, to plan out every detail for the next three weeks, 2 months, whatever amount of time, ahead. But as soon as you start playing scenarios in your head, it's over. It branches and branches, even from day one, and it's too overwhelming for your brain to handle. Even if you're some sort of genius, unless you've done this thing you're planning a thousand times, it'll never go the way you want it to and will most likely diverge in ways you've never even thought of.

I used to love making to-do lists. The act in itself was hilariously fun, but only for the first few times I load up whatever new software I was trying. I'd spend a couple hours dumping my brain into the thing and relief would wash over me as I suddenly became unburdened from all these things floating around in my head. The thing is, after a couple days, my head would fill itself right back up with even more things until it reached its limit. So coupled with an impossibly long list to complete, I'd be even more pressed for time for all the things I wanted to do and I'd eventually just abandon everything.

But there's still hope with thinking ahead. It's something you have to do, or else you're just walking around not knowing when you're going to walk into a pole or off a cliff. It's about a strategy. Try taking out the nitpicky details from your plan, but keep that strong notion on which direction is forward and up. 

I happen to have a favorite strategy. Try to keep a streak alive. Do this one thing, once a day, for as many days in a row as you can. Keep it simple, yet keep it aligned with one of your goals. So you have your long term goals and that's as far as you plan ahead in terms of specifics. Then each day, think of something that you can tick off in your calendar. So say you're thinking of building a website, and you have no idea how. Make it your task to open your favorite text editor and code for an hour a day, following along a tutorial in a book, or struggling to build your own thing. Even if you get nowhere, you're better for it, and you can cross out that day to keep the chain going.

This is applicable to so many things. Trying to lose weight? Don't think, "I'm going to lose 10kg this month, and this is how I'm going to do it every day for the next 30 days." One thing comes up to mess up your plan and you'll feel it's okay to push it to another month. Instead, each day, make your task to simply put on exercise clothes and shoes. At that point you've already resigned to do *something*, even if it isn't much. You can even put them on and take them off right away and still check off the day, only because I know you'd feel silly for doing that.

This also works for those one-off projects that won't turn into habits. For example, take that big new assignment at work. You're obviously going to be working on it every day... but there are some days where you're just not feeling up to it. Do it anyway. Those are the days that add up to you getting it done early or on time or late. This works wonders for those side projects of yours too, after work and on the weekends. Working on it for five minutes is much more than five times better than not working on it at all. Don't do more than one at a time though. You've only got so much energy (and time, but that's not as important is it) in a day.

Finally, start slow. Don't throw in eight things you want to do a day from day one. You mess up one and you'll drop all of them. I suggest going through your days as usual, but add one thing per month. Think about it. At the end of the year, if you're diligent, you'll be doing 12 amazing things every day.

And the first thing I always suggest for month 1? Floss every day. Seriously, it's so important.
