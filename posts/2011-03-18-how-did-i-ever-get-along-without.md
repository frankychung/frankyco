---
layout: article
title: How did I ever get along without
slug: how-did-i-ever-get-along-without
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: vim coding
category: article
---
Vim is a text editor and I love it. How do you love a text editor? How can you get any dorkier?

I used to like coding. It's fun to see numbers and letters turn into something so much more. But now, thanks to Vim, I *love* coding. Have you ever dreamt you could fly? There's nothing like it, but you can have a taste of it when you code with Vim.

No matter where your cursor is, a few keystrokes will take you to anywhere in the document. After a bit of practice your fingers will act without you having to think. You will be moving, zapping, and creating entire blocks of code and weaving in and out of different files with ease.

Another lovely thing about Vim is that you can make it yours. Keyboard shortcuts, plugin settings, color, general Vim settings, everything. The huge selection of plugins is awesome too. I have a few I couldn't live without. But then, you can jump on any computer fresh and still feel at home.

Finally, it's a continuous learning process. I've only scratched the surface of what's possible in the past few months and I'm already loving it. It's one of those things where you think, how did I ever get along without it?
