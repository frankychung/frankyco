---
layout: article
title: Break time
slug: break-time
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: productivity work
category: article
---
It's hard to take breaks. What? That's right. When you're in the zone, it doesn't feel right to stop. The thing is, it doesn't feel right when you're out of the zone either. Like when you've been surfing the web for an hour. Time sneaks up on you like that, so take breaks.

I'm a fan of the [Pomodoro technique](http://www.pomodorotechnique.com). But like most of these productivity hack things, they're hard to stick. You realize they're not hacks at all, and if they are they probably aren't worth your time. Pomodoro actually takes a lot out of you and you're more likely to mess it up than not in the course of a day.

Basically, take a five minute break after twenty five minutes. After two hours, take a longer break, for about half an hour. The rest is a bit more involved, like logging how many twenty five minute sessions you do, estimating how long things will take, being pretty unforgiving if you get distracted in the middle of a session, things like that. But the core of the whole thing, to take breaks, really works. 

I've read variants of this elsewhere. My favorite is [Cal Newport's blog](http://calnewport.com/blog/) and books. I'm pretty sure he never references Pomodoro, which is reassuring because that means other people who are successful with their time naturally stumble on the same thing. The time ranges differ but it seems to be a common theme, that you need to get up and take regular breaks, even if you're in the middle of something. Get a drink. Do some pushups. Stretch. Anything to take your mind off for a very short time. Don't worry, you won't lose your place.

It's easy in the beginning because it's novel, but it wears on you. Sometimes you dont know what to do in your five minutes. Sometimes you snooze the buzz just to get five more minutes in, and you forget about it for the next two hours. 

But there are the days I manage to follow through with something so simple &mdash; take regular breaks. Those days feel incredible. They feel like the day just flowed. Which is counter-intuitive because I was actually spacing it out with little breaks. But that doesn't stop the flow. What actually stops the flow is when you sit there for too long and you get stuck. You get stuck and then you get tired and frustrated. When you're taking breaks, you don't have the time to get stuck. It's like a little reset. You reset and you land on something that you can immediately start working on. 

In fact, I almost look forward to getting my break time when I'm in the middle of something. I know exactly where to start when I come back fresh. And then from there I ramp into another twenty five minutes of good work. Seriously, it's awesome.

What can you do in five minutes? A surprising amount. Get up and make a cup of tea. Stretch. Tidy something up. Or read. Reading is my favorite. I've read entire books in those five minute bursts throughout the day. When I get really far in a book, that ends up being a perfect day &mdash; constantly interrupting myself when I'm in the middle of something to read a few pages of the book I'm currently enamored with.

And even if you're just reading, get up. You know how they say sitting is deadly. I don't know why, but I don't mind believing it. It's the perfect excuse to stop and get up every so often. Isn't it easier when it's a matter of life or death? 

What do you do during the longer breaks? I tend to take a bit longer than half an hour. Sometimes I go for a jog. If it's around a certain time in the afternoon, I'll take a nap. And it's the perfect amount of time to cook a meal. 

Still, like I said, it's hard. But if you're on a Mac, I found an app that actually helps with this whole thing. It's hard to imagine why you'd need help to stop what you're doing. But this thing puts you in shutdown mode so you don't have much choice, as long as you give it permissioin to do so ahead of time. It's called BreakTime. I'm pretty sure it was 3 dollars. It's amazing how this thing I use every day cost only 3 dollars.

It's simple. You designate the time period you will work. Then you designate a break period. When you hit that break, it comes up to the front, doesn't let you switch apps, and doesn't give you an option to end the break timer early. It throws you off the computer for five minutes (or whatever you prefer). Okay, it lets you hit the snooze button, but really, just take the break. Especially if you're in the middle of something.

It's almost like a game. Can I get to 16 today? It doesn't matter how long it takes. On the days that I accomplish this monumental task, it takes me an average of 12 hours. That's 2 hours more than it should take, which means I take twice as many breaks as this system is supposed to let me have. But that's okay. There's hardly a better feeling knowing you did 8 hours of solid, good work in a single day. I don't think it's something you can do every day. If you're working that hard, you need the weekends off. But what if you did it most days? What crazy amount of stuff would you get done? 

What if you work at a 9 to 5? That's fine. With this system, in those 8 hours, you have 6.5 hours of solid work and 1.5 hours of rest time. Well, a little less than 6.5 because of the 5 minute breaks, which is okay. When's the last time you got 6.5 hours of pure work in a day at the office (if you go to an office, of course). And even then, you've got another 1.5 to spare before you reach that monomental 16. So try to fill it up. Work on a side project. Go home and create something. With 1.5 hours, you've got plenty of time to do whatever else you want to do in the day. 

Interrupt yourself 16 times in a day. Just try it. It'll be hard, and you probably won't make it there the first time. But when you get there, end it on a good note &mdash; when you're in the middle of something. Hemingway said the same thing, right? 
