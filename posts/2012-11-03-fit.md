---
layout: article
title: Fit
slug: fit
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: design
category: article
---

You know what they say about clothes, that it's all about the fit
and quality? I hadn't been so keen on this, half because I was lucky
enough that half my clothes happened to fit pretty well, half because
I couldn't be bothered to think so much about my clothes. But then a
period of time passed where I didn't purchase a single new article of
clothing (except for a few insignficant t-shirts here and there) for
almost two years. I didn't realize it but one day it hit me. My tastes
had changed. I didn't like what I was wearing. I didn't like what I had
in my closet. It became a nagging something in the back of my mind. 

Then the second big thing hit me. I finally got why people get 
worked up about fashion. Why there are whole careers and industries
based on clothes. Because when you lack the right clothes to express
yourself, and you feel that's a good way to express yourself, you don't
feel so great. And when you can finally get that expression back, you
feel amazing. Like a million bucks. It's one of the best ways to shout
out to the world what kind of person you are. It's also unfortunately
one of the easiest ways to show what kind of person you are, if you
aren't careful.

So finding the right clothes brings us back to fit and quality. You want
good quality for obvious reasons. But fit has only recently started to
reveal itself to me how important it is.

A good fit means there's nothing there that doesn't need to be there.
It sits just right. It also means there isn't something missing. This
takes a bit of experimentation, to find what's the best fit for you. And
it might change over time. But when it's just right, it disappears. You
stop thinking about it. It's effortlessly there, and you go on with your
day. Now think about when it isn't just right. If it's too loose, you
feel this odd sensation of wasted space, of excess, of weight. And if
it's too tight, you feel suffocated. It feels revealing. In both cases,
the feeling is glaringly there, and it take up precious space in your
mind. You wish it wasn't so unfitting, and you worry if other people
notice the same thing, and you dream about the right pair of pants, or
a better shirt. When you're good, you can concentrate on other things.

Think about how this applies to everything else in your life. Take
eating. When you eat too much, you feel like shit, and when you don't eat
enough, you're itching for more. There's that moment of just enough food,
or maybe just enough of the right food. That's fit.

Take a conversation. A good one. One that flows effortlessly back and
forth, no awkward silences, and no one glancing at their watches as the
other talks too much. That's a fit conversation. Take design. What is
good design? That's a hard question, but some would say it's to strip
something to its essence, to make something easy to understand and easy
to use. That's fit design.

So go out there, carve away the excess, and show the world the best
version of yourself.
