---
layout: article
title: Lovebirds
slug: lovebirds
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: love work
category: article
---
A girlfriend is a second job. That's not a bad thing. If you work hard it will be rewarding. You can't sit back and expect everything to come easily. And thinking about it the other way around, you should be picky about your job like you're picky with who you date. Don't accept whatever job just for a paycheck.

How about love at first sight? Even if you really vibe with someone from the beginning, you got to put in the perspiration to get something meaningful out of it. It's like when you have a knack for something. Maybe you find out you have good design sense, or you're good with numbers. You still haven't started yet. You need to work to get any good at it. 

If you're not working hard at it, you're not getting much out of it either. You feel unfulfilled and you think maybe you're on the wrong path. If you're merely doing what it takes to get by, instead of trying to to do the best work possible, you're going to start to hate going in every day. But don't blame it on them yet. Figure out all the things that you can control and try to shake it up. Who knows, you could find something great that was there all along. Bring something special to the table no matter what's pushing you back. Only after this, and you're still getting nowhere, does it make sense to consider moving on.

Now, if you love the job you got, you'll work hard to keep it right? You can't go home after a long day and expect to be able to veg out, thinking you deserve some time off from her once in a while. It's your second shift. Change your mindset and the work itself becomes the gratification. Sure, you got to sacrifice some things from the single life, but it's just like that new thing you're really excited about. You're probably going to drop the excessive amounts of TV if you're out to build something great.
