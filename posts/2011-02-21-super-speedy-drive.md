---
layout: article
title: Super speedy drive
slug: super-speedy-drive
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: ssd
category: article
---
About five months ago I found a deal on a solid state hard drive. I had been hearing a lot about how people have been moving to SSDs for a while. Everyone says it's the single best upgrade you can make for an aging device. I never bit the bullet since I didn't have an old machine, and I couldn't imagine that saving a second here and there would make me a much happier person.

Wow, was I wrong. I am much, much happier. When you spend the majority of your day in front of a computer, that snappiness is such a breath of fresh air. Years and years of two minute boot ups, one minute Photoshop loads, five to ten second application loads, opening and closing multiple programs every hour, all of that stuff you didn't know was kind of annoying can be obliterated with a single change. Even today I marvel at how quickly my computer wakes up from sleep. Sometimes I open Illustrator just to watch how fast it's ready to go.

It's like a really fast website versus a merely OK website. When you click that link, a half second can make all the difference of making it feel sluggish or lighting quick.

Unfortunately that's not the entire story. SSDs are expensive (to me). When I found this special deal, I decided to look at it purely in terms of price, which was foolish. I got the lowest capacity which was a quarter of what I was using before. I sort of relished the idea that I would have to force myself to be more picky and minimalistic on what I put in my computer. I would have to pare down my music collection. No more movies living there for months never to be watched again. All those useless applications would have to go. It would be like starting with a clean slate.

But 40 GB goes by quickly, faster than you would ever think. I remember I had 40 GB in 2003. Even then I had a difficult time managing it. Now I'm constantly living on the edge. I keep getting warning messages that my startup disk is running low on space and some dangerous thing that I don't understand is going happen. No matter what, no matter what I delete, even if I make huge 2-5 GB increases by drastically changing part of my daily workflow (I switched from using Apple Mail to going completely online with Gmail), within a week it goes back down to danger levels of under 1 GB. It's like how the time it takes to do something expands to how much time you give yourself. There's some fancy name for that law which I can't remember but it's pretty much that.

Still, I think it was the best purchase of 2010. I can't stand using other people's computers anymore. That sounds kind of snobbish, and it is, but the times they are a-changin'. Sure, it doesn't directly help me do awesome work, but it makes me so, so happy (which, now that I think about, actually might help me do awesome work). There's no going back!
