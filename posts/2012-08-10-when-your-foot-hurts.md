---
layout: article
title: When your foot hurts, everything hurts
slug: when-your-foot-hurts
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life
category: article
---
It's hard to grasp what I mean by that until it happens to you. I have a debilitating symptom on my left foot that makes it painful to walk. It should go away in a couple weeks but in the meantime, it's all I can think about. That and how much I miss being able to walk normally. How much I miss wearing sandals (because in my left foot's current state, theres no way I'm going out in sandals). How I just bought new shoes and I can't wear them yet. 

This happened to me before when I was in elementary school. There was something on the balls of my right foot and the doctor had to zap it off with a laser. Back then, it was all I could think about too. I would keep looking at it, willing it to go away. It was painful in the same way that no matter what I'm working on, no matter what I have to worry about, all of it is eclipsed by the thoughts of my poor foot and how much I hate having it in such a condition. I take it for granted.

I remember a period of three months when I had an intense tooth ache. Or all those years as a kid when I would have allergies and couldn't breathe through my nose at night. Now that these things are fine, I forget about how precious they are and how amazing it feels to be okay. Same goes for the short term too, like when you have a fever, or when you're hungover. What I wouldn't give at those moments just to feel normal.

It only takes one thing to knock a giant like you and me down. Everything might be running smoothly right now but still, you got to take care of yourself. Get a check up at the doctor. Exercise. Stay healthy. And treat your things well. It only takes one thing to make your car stop running. Or one thing in your house to break to rack up a bill. One thing to sour a relationship, or one thing to ruin your day. And think about the things you work on. The good work you do. Is there something that's not working right? Find that one thing and yank it out.

And that's what's hopeful about things that aren't going right. There's probably one big thing you can do to make it better. To bring things back to normal or how you'd like it to be. There's nothing like waking up in the morning and looking at my feet and seeing that it's just a little bit better than the day before. It's that reassurement that when this is all over, things are going to be okay. 
