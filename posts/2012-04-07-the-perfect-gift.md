---
layout: article
title: The perfect gift
slug: the-perfect-gift
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: books
category: article
---
On my birthday last year, I'm pretty sure nobody had any idea what to
get me. I know this because I got the weirdest gifts this year. They
were thoughtful of course, and I am very lucky to have people that go
out and buy things for me on this special day, but you could tell, these
friends of mine struggled to figure out what to get me.

The thing is, the things I do want, they are so much simpler than the
things I actually got. I never sat down to think what I would love to
get on my birthday. So when I got these gifts and I looked at them and
thought, I don't really know what to do with these things, I thought
hard abuot what would have been nice, since I figured I could treat
myself to something on this mostly arbitrary day.

I thought about computer stuff, maybe some video games, clothes, and
some other fun things, but it didn't take long for me to realize what
was the perfect gift for me. *Books*. I have a huge list of books I want
to read. I thought about how good it feels whenever I get a new book,
that fresh book smell. I mean, lately I've only been getting books for my
Kindle app, but besides the smell thing, the feeling of starting fresh
on a new book feels the same.

And that is a relief. That when I sit down and think about it, that
thing I love, looking at it from the outside, is such a great thing
to love. How I would love to live somewhere that's just surrounded by
books. How I hope my family will grow up with books, in the face of an
age where so much of our reading comes from a devices, if we even
read at all.
