---
layout: article
title: Holding the world in your hands
slug: holding-the-world-in-your-hands
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: apple ipad
category: article
---
I bought my iPad just over 6 months ago as I write this. I look at my other gadgets gathering dust and pat myself on the back for sticking with this one.

The thing is a great tablet. I have a lot of fun doing things you can do on your regular computer with a touch interface. Dragging and panning and zooming and whatnot is a very novel experience and, in most cases, better than its mouse and keyboard equivalent. Plus it's nice how I don't have to unplug it from the wall or pack it in some protective case. It slips in my bag nicely and carries well around the house. 

But when I turn it 90 degrees and hold it like a book, it turns into something entirely different, something so natural and seamless it becomes an absolute joy to use. Going back to anything else is almost lackluster. It isn't just surfing the web like [Mr. Jobs pined on about while browsing the NY Times website](http://www.youtube.com/watch?v=7qhDtcbxnak). It's _everything_. Checking my mail, reading books and PDFs, checking my serious time-suck number of feeds, playing some hilarious game, heck, even browsing my music library. When I switch back to doing the same things in landscape mode I feel the pull back to reality.

I think it has something to do with how I can hold it in one hand and interact with the device with the other. With the other orientation, it makes more sense to put it down or at least hold it with two hands. Not that I'm not knocking landscape. If you type at all on this thing, which I do a lot and am fact doing so now, you got to use it longways and on the table. I'm not trashing keyboards either (although I can do without the mouse). There are so many things you can only do properly on a regular computer. But with the things that you can do on both, the tablet is usually better, and it's the jump to how phenomenal it becomes when you hold it upright that have made my friends tired of hearing me rave about it.
