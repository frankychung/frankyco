---
layout: article
title: A blank page
slug: a-blank-page
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work creativity fear
category: article
---

Facing a blank page is scary. It's the same for any kind of work, be
it a blank sheet of paper, a blank canvas, or a blank screen. Even the
small things are hard, like the blank page in your journal, or the blank
box for a a new email, or even the blinking cursor for the beginnings of
a new text message.

It's scary because you can't picture how it's going to turn out. You
might have a vague outline of what it will look like, but you can't know
exactly until it's actually there. It's like looking into a dark tunnel
and only seeing the faint light at the end. You imagine the bumps in the
road and the inevitable curves and twists you have learn to navigate
along the way.

You've done good work before. You do good work consistently. But you
still can't shake the fact that this time, it might not be good. Either
as good as you want, or any good at all. That's okay though, it doesn't
have to be. Great things come from when you make something and go back
and make it better. They very rarely come out of thin air the first time
(count those times lucky).

But haven't you found that every time you even try, it feels really
good? When you're done with the first version of something, it's almost
always *good*. It could be better, and it could be great, but it's
always better than you think it was going to be. It's a victory. You
took a blank page and turned it into something. Something useful,
something beautiful, something unique. Savor that feeling. Look forward
to that feeling. Use it to fuel your work. Your work on new things, and
your work on making good things great. Know that anything is better than
abandoning a blank page.

