---
layout: article
title: Compatible
slug: compatible
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: friendship conversations books movies love
category: article
---

You know those people that you seem to be able to talk endlessly about
anything with? Even if it's about nothing. You could shoot the shit
forever. You're constantly making witty remarks and inside jokes and
obscure cultural references. It's like an exercise in talking and it is
amazing.

These people are rare. With most people, you have to make at least a bit
of an effort to keep the conversation going, and I'm guessing they're
making a bit of an effort too. But it's still mostly pleasant with most
people. You talk about your day, your work, and it's always fascinating
to learn more about someone, so you also manage to talk mostly about
them. Maybe they know that too and then suddenly you're mostly talking
about yourself. But still, with most people, you don't get that high
from a conversation. I think you can get better at it, so you can get
it with more and more people, but there are the people where it comes
natural. Treasure those people. Talk to them as much as possible.

The funny thing is, the easier it is to talk to someone, the easier
it is to let it go quiet. Take a normal car ride. The easier the
conversation comes, the easier it is to ride in silence. The harder it
is to keep the convsation going, the more awkward the silence becomes
when it does inevitably creep up. 

The strange thing is that the people you have the best conversations
with don't necessarily end up becoming your best friends, or friends
at all. Sometimes I meet someone and we have a blast and are laughing
all day, but then I don't really ever see him again. And with some of
my best friends, I sometimes still feel awkward sitting in the car with
them if we've run out of things to talk about.

But maybe that's a good thing. It could be too exhausting to hang out
with these people all the time. When you have an awesome conversation,
it feels like your mind is going a mile a minute. Maybe it's good your
best friends don't all love the same movies and same books you do, so
you don't have to talk about them all the time. Maybe it's good your
best friends don't have the same sense of humor you do, so you don't
have to be laughing all the time. You definitely need it, but you need
the quiet as well.

So treasure the fact that these people exist. Know that everywhere,
there are people you connect with. And try your best to find them. But
treasure your friends too, for which you are friends for all different
kinds of reasons. And try new things too. The best conversations are the
ones in your comfort zones. If you get outside of it, you just might be
able to expand it.
