---
layout: article
title: Make it easy
slug: make-it-easy
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: productivity food coffee
category: article
---

The easier something is to clean, the more likely I am to do it. I'm
pretty good about most things, except when I get in a funk about it. For
example, I would rather cook than buy take out on a normal night, but
sometimes, I'll let dishes pile, pans go unwashed, trash pile up, that
I'll become relunctant to head into the kitchen. It's not hard, and it
doesn't even take that long, but it's the thought of doing it that stops
me. So the less time I think something will take to do, the less I let
it fester in my head, and the easier it is to just do it.

Think about how you can minimize thinking about anything before doing
it. You might need to do it a different way, or use a different tool.
Take, for example, coffee. I love coffee. Probably too much. But coffee
machines are a pain. A pain to make to make coffee with, and a pain to
clean. Then I got an Aeropress. Wow that thing is easy. I'm still blown
away every time I'm done making a cup and it's time to clean it because
it takes like three seconds. *Three seconds*. I drink so much coffee
now. I'm pretty sure that matters more than how much better it tastes.

There's stuff that is always going to be scary when you think about
it. Scary as in you freak yourself out by imagining yourself having to
do it, which is a lot worse than actually doing it. Let's go back to
cooking. If you're trying to eat healthy and maybe save a few bucks
and maybe get better at taking care of yourself, cooking is amazing.
But you can't make a gourmet meal every day (okay, unless you're into
that). Find the simple recipes and buy the simple ingredients. There
are an infinite number of meals you can prepare in 10 minutes. Get the
right tools to make it as fast and easy to clean as possible. Here's one
thing I do &mdash; I always buy the pre-cut meat because I fucking hate
cutting meat. Yeah, I'm a weirdo.

Or take exercise. An hour at the gym is daunting. Driving there,
stretching, eating right, all that shit. So figure out how to make it
take thirty minutes. Twenty minutes. Figure out the stuff that does
80% of what you need and just do those, quickly, and get out. Find
and experiment your routine so that from out the door to back in and
showered it's thirty minutes. It's hard, but doable, and it only gets
easier.

Saying all that, unfortunately, I still haven't figured out how to wash
and fold laundry any faster than I've been doing it for the last ten
years.
