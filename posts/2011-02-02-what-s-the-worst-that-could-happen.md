---
layout: article
title: What's the worst that could happen?
slug: what-s-the-worst-that-could-happen
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: haircut productivity
category: article
---
I've been cutting my own hair for about five years now. It started in high school after a traumatizing series of bad haircuts. When I finally admitted to myself that there are in fact plenty of professionals that can give me a haircut better than me, I had been doing it for so long already that I thought I might as well get as good at it as I can. Turns out there are some surprising parallels with cutting my own hair and the sort of projects I'm doing now.

After a few goes at it, I realized that I need to prepare for the fact that I won't get it perfect the first time. I don't think that's something you expect from a haircut. And you shouldn't, if you're sitting in a salon. But when you're staring at yourself in the same mirror you use to brush your teeth, haphazardly hacking away at that precious lump of hair on top of your head, it's a little harder to knock it out of the park every time. 

With that simple thought, that there will be another revision down the line, later that day or the next, you approach things differently. You snip so that there is always room to make a bit of change or adjustment. Before, I used to just go all out and cut it really short, partially out of impatience, partially out of anger, but mostly because I didn't know better. Don't mess with the little details the first time around. Take a step back to give it time to settle in. It's a lot easier to _see_ what changes you need to make than try to _guess_ what changes you need to make. You tend to go overboard if you don't take a break.

Think of it this way. Mold the hair to a general shape and volume you want. Then you chip away at it later. That's the bulk of the work. To go from that first 70-80% to 95%. You have to settle with leaving that last five out or you'll probably go crazy.

By giving myself some room to mess up, I gave myself the permission to try new things. There were aspects of cutting my own hair that I naturally learned over the years purely from thinking, what if I do this, instead of that which I've done the last three haircuts which I wasn't so happy with? Those two or three big revelations from learning from my mistakes were really powerful. I've reached a level of proficiency miles ahead from when I started by being very careful not to repeat stupid mess-ups and creating little safety buffers on risky ideas.

I'm still approaching the perfect haircut. I'll probably never actually get to perfect, but that's fine with me. It's a good feeling when you're done and you realize it's the best haircut you've done yet.

Nobody cuts their own hair (or wants to). But if you're a weirdo like me, to psyche yourself up to actually do it, you got to think, what's the worst that could happen? Be prepared to fix up the inevitable mess you make and it's a lot easier to start.
