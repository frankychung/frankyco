---
layout: article
title: Wallpapers
slug: wallpapers
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: wallpapers iphone ipad
category: article
---
We're a product of everything we surround ourselves with. I've been hearing that a lot lately. Like the city you live in, the people around you, the music you listen to. A lot of it is out of your control, but there's some of it that you have 100% say in. Why not take advantage of it?

Where do you spend most of your time? There's a high chance you're on a computer all day. One of the easiest ways to keep things fresh is to choose a great wallpaper. A wallpaper can affect the whole mood of a computer. Something beautiful or serene can put you at peace. Something clever and simple can put you in a creative mood. I have a friend who adorns his screen with his favorite Korean girl band. It makes me want to get off his computer as fast as possible. So you see, a wallpaper is powerful.

I actually have a love-hate relationship with my computer. I love that it enables me to do work that I love, but I hate how much time I spend on the thing and how wed I am to it. So even though I'm on it for the majority of the day, I try to spend as little time on it as possible, if that makes any sense. That means, besides any necessary work that I have to do on my computer, I try not to sink time into Facebook or Twitter or surfing the web randomly on my computer. My wallpaper is a solid dark gray. That's it. It's bland and boring, so I don't think about it, so I don't grow any more attached to it than I have to.

I'm still addicted to Facebook and Twitter (how could you not be), so where do I relegate all my fun to? My iPhone and iPad. On those two things, I'm quite particular on changing my wallpaper up to something awesome every couple of days.

I dont Google iPhone or iPad wallpapers. I mean, that would probably work fine, but like so many things, if I can be a little more eccentric, why not? So every few days, I scour [ffffound](http://ffffound.com). I like to [save a few images here and there for my personal collection](http://franky.co/2011/03/little-snaps) and some of them are perfect for wallpapers.

They are nowhere near the right dimensions or proportions. But that's okay. I pick the images that look good against a black background, which mostly means they have a black background, so it integrates seamlessly into the black of the phone. 

Plus I like the fact that every beautiful image I have on my lock and home screen is likely not being used on any other lock or home screen in the world. Okay, maybe there are some people who are doing the same thing as me. ffffound is pretty huge after all. But like me, they're probably doing it as their own little secret, not particularly to keep it a secret, but because there's no real point in telling anyone. 

So I decided to do something about that. I made [this](http://ps.franky.co). It's useful for me, so hopefully it'll be useful to at least one other person out there. That would be enough to make it worth having made it.
