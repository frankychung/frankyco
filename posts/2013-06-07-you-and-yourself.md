---
layout: article
title: You and yourself
slug: you-and-yourself
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: friendship personaldevelopment
category: article
---

It's so great to have a good time with friends that it's hard to
imagine sharing these kinds of experiences without them. Think of the
trips, the parties, the nights spent talking until the sun rises, the
comaraderie of going through a high pressure situation. Now think of the
smaller things, like a good meal, a movie, even a drive to nowhere
in particular. When you're sitting at home, and you've got a whole
day ahead of you with nothing planned, it's the natural thing to call
someone up to do *something*.

I'm scared to do it. To walk out the door with the full intention of
spending quality time with myself. It's not that I need time to think,
I do plenty of that, and maybe even too much. It's that I'm scared of
running into a experience that, and it happens more often than not, I'm
glad that I got to share with someone else being there and actually not
having someone there.

It's unnatural. If I'm in my car, and I'm not running an errand, I feel
weird passing time without being with someone else or on my way to being
with someone else. It's this feeling that, if I'm hanging out with
myself, that I and everyone around me will think I'm just someone who
doesn't have enough friends. And that's horseshit.  

I have the best friends in the world. So do you. Don't ever think
otherwise. And they're lucky to have you. With this in mind, try
spending some time with yourself. Spend time with yourself like you
would with your best friend. 

I got the first taste of this when I started freelancing. I loved
working at places with free wifi around where I lived. Places like
Starbucks and Corner Bakery. Working for yourself can drive you a bit
crazy if you stay at home all the time, so I liked having *people*
around, even though they're passing through and having nothing to do
with me and I'd never talk to any of them. I'd give myself the excuse to
go there to do work. But suddenly, I found myself going there and after
a good bit of work, sometimes I'd sit back to enjoy a cup of coffee
and read the latest book I was on. And I'd find myself walking around
afterwards doing a little shopping. It was *relaxing*.

I have a good friend who does this all the time. She *loves* going to a
cafe and staying there for hours, enjoying a sweet pastry and a drink.
To read a book, to walk around and watch people. But it goes beyond
this. She loves to go on trips by herself. Like you and me, she doesn't
have a shortage of good friends, but she needs this time with herself.
To explore somewhere she's never been, to lay on the beach, to see the
sights, by herself, sometimes. She'll never turn down an invitation,
but it's completely up in the air if she's going to spend a day by
herself or not. That's amazing. For me, it used to be, if I'm not going
somewhere with someone, I'm not going anywhere. I'm not saying there's
anything wrong with this, because we have an infinite amount of work to
do, and an infinite amount of things to entertain us at home, but that's
not spending time *with yourself*.

Another good friend told me the other day something interesting he
read. That although not many people do it, one of the most surprisingly
satisfying things to *do* is watch a movie by yourself. At the cinemas I
mean. Surprisingly, very little is lost if your friends aren't there.
In fact, there are some aspects of it that are a little better. You
don't have to worry, if you were the one that picked the movie, if your
friends are liking it. You don't have to get irked if some of them have
annoying habits, or like to talk. You don't have to worry about finding
the right seats for a certain number of people, because you can fit your
one person almost anywhere, even in a packed show. You get the same
experience of immersing yourself in a movie in a large dark room if
you're alone or not.

And I tried it. It was exactly like he said. I saw a movie I was itching
to see for over a year by myself, something that would have been a sort
of event with my friends back home, but due to circumstances I couldn't
wait and had to see it by myself. It was awesome. I walked out of the
theater with the exact same feeling I get when I watch a great movie. In
fact, most of those times, I'm so invigorated and exhausted I kind of
*don't want to* talk to anyone. It was a relief.

So do yourself a favor and *try it*. It's not going to be for everyone,
but go to a museum, take a drive, do some shopping, eat a meal, watch
a movie, not *by yourself*, but *with yourself*. After all, you're the
only person that you're forced to be with every waking moment, so you
might as well try to enjoy it.
