---
layout: article
title: Interesting
slug: interesting
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: friendship conversations life
category: article
---

The moment you've said everything on your mind, suddenly, you've become
uninteresting. It's because when we think we've got something figured
out, we'll be itching to find the next thing to figure out. When you get
handed something on a plate, it's a lot less satisfying than digging it
out on your own. And even when you do get it, it's not like you're going
to sit around. It's not like you're ever going to be satisfied with what
you have.

I don't necessarily mean be mysterious. It's just, be interesting.
If I can tell what you're thinking, or you're telling me what you're
thinking, you've done all the work for me. It's no fun. I'll move on to
the next thing that I can work on.

When you're working on something, you need signs of progress. Don't you
love finding out something new about someone after knowing them for a
year already? Or when they change their mind about something? Or when
they comfort you with a familiar subtle behavior that you figured out on
your own?

The thing is, this interestingness, it can go on forever. Aren't we
always learning, always changing? If you can keep it up, to become
a better, cooler version of yourself every day, then you're always
going to be awesome. 

And besides, meaningful conversations are born from the thoughts and
experiences of two or more people, and often in the moment. You can have
the most amazing conversation in the world and not have someone know a
thing about you.

Sure, say what's on your mind. Just not all of it. Just enough for
the situation. Let them try to understand you. Drop hints. Not just
through words, but actions. Facial expressions. Posture. Work ethic. Ask
questions. Listen more than talk. Smile more than not. Work more than
not. Laugh and joke. Then be serious. Keep 'em guessing. They'll want to
hang around to see what's coming next.
