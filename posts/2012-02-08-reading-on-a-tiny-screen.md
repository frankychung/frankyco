---
layout: article
title: Reading on a tiny screen
slug: reading-on-a-tiny-screen
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: iphone reading kindle 1q84 infinitejest instapaper dfw
category: article
---
I recently read a 900 page novel entirely on my iPhone (1Q84 if you're curious, with the Kindle app, if you're curious). It was awesome. I never thought I could enjoy reading something so long on such a tiny screen, especially compared to holding the real thing in my hand.

But it makes sense. My favorite (maybe second favorite) app is Instapaper. Reading things you've saved in Instapaper is one of the best ways to fill in those pockets of dead time throughout the day. Around the end of last year, a lot of recommended reading lists for these kinds of apps came out. Really lengthy pieces, from magazines that I don't usually read but are really good. I had no qualms with reading these on my iPhone, but I didn't realize then that if I'm willing to read a five thousand word story on that tiny screen, that suddenly becomes no different than reading a nine hundred page story, compared to the few hundred word articles I'm used to consuming quickly.

After I finished a few of them and came away with that familiar glow after reading something great, I decided, on a whim, to load up that long novel I recently bought on Amazon. It half felt like I was joking to myself and that I'd only read a few pages on it. Next thing I knew an hour passed and I had finished the first few chapters.

There are two things that make it actually a pleasure and almost an advantage to read on the iPhone. The first is the amazing screen. I can't get enough of the retina display. Sometimes I put the screen right up to my eye to try to find that hint of pixel but all I ever find is blackness. It's beautiful to look at, and for some reason that improves the experience (not to say the iPad isn't a wonderful experience too, although imagine it with a retina display... Drool). Actually, now that I think about it, there were times when I was sitting at my desk _using my iPad_ and I still opted to pull out my phone to read a book.

The second is that it fits in your pocket. This was a revelation to me. Now all those times on the train, waiting in line, taking a break, you have a book right in your pocket, accessible at any moment. You can do a surprising amount of reading in five minutes, and what better way to spend five minutes? Plus, think about that 900 pager you read once in a while. All that heft crammed into zero grams in your phone is pretty amazing. And you're reading all of that with _one hand_. Whatever you're able to do with one hand is a thousand times more portable than anything that requires both.

I guess I always imagined that with such a small screen, you'd be too focused on any one part of the page and you'd be too busy flipping pages to be able to remember the characters and process what was going on in the story, compared to one off articles you save off the web which usually only has one or two points to convey anyway. So wrong. I lose myself in the story just like I do holding any paperback. Besides, I never noticed myself flipping pages on the real thing anyway.

I'm on my third book (Infinite Jest, if you're curious) in a surprisingly short amount of time. Try it. You might just start reading more.
