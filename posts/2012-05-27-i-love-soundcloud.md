---
layout: article
title: I love SoundCloud
slug: i-love-soundcloud
image: ''
embed: ''
link_url: ''
footnotes: Check out [my favorites on SoundCloud](http://soundcloud.com/franky-chung/favorites).
tags: music soundcloud
category: article
---
Social networks are the easiest way to reach friends, store and share pictures, and it's actually a pretty good content discovery tool (I'm mainly talking about Twitter). But there's one I love that's different from the rest. It's like that cool thing you're into that none of your friends know or care about, like the geeky hobby you have that's full of people you secretly admire. I'm talking about [SoundCloud](http://soundcloud.com).

I heard about SoundCloud a while ago. I didn't get it. Why would I want to share my sounds? Who is sharing their sounds? Why would musicians share their music on this thing? I thought about all this for two minutes and forgot about it for a long time.

It's been about a year now since I became obsessed with [Designers.mx](http://designers.mx). I discover so much great music and even entire new genres on there. At some point I heard an amazing song and googled the name to see if there were any more by the same artist. A link to SoundCloud was the first result. I must have clicked the Facebook connect button while browsing around because suddenly I had an account and could save songs to my favorites. It was a slippery slope from there.

The key to enjoying what SoundCloud has to offer is to get in the mindset that you don't have to share any sounds. At least I don't plan to, because I don't have any. If you're in the same situation, then once you realize it's okay, you can have so much fun on the thing.

From that first song, I discovered an amazing collection of music from a single artist. So I followed him, since those follow buttons are so enticing to press. My dashboard filled with a few things this guy favorited himself. I clicked through on some of them and what do you know, they were good too. It felt like I was navigating an unfamiliar world, hunting for treasures. Every new song I added to my favorites was a gem. It was fun.

It occurred to me then that there are so many people making so much good music that never had a good place to share it until SoundCloud came around. There are so many people making this music that I love so much in their rooms, in their basements, in their free time, and it is a skill I don't even know where to begin trying to comprehend how they do, how they learn it, even what skills you need to know to do it. I can give you a good rundown on the skills, the motivation and the background needed for building web applications, but I'm clueless when it comes to creating music. So I'm so grateful that SoundCloud opened this mysterious world for me, and for letting me observe it at a safe distance.

Now I always search for new artists on SoundCloud first if I hear them somewhere else. If they're not a major label artist, they're likely on SoundCloud, and at this point I've saved a pretty eclectic mix of artists and songs. I might treasure the things I've collected in my SoundCloud more than my Twitter or Facebook. I used to do something similar with YouTube, saving my favorite songs, but this feels infinitely more legitimate. First of all, on SoundCloud, it's usually the actual artist who uploads some or all of their songs. Secondly, there are plenty of songs on SoundCloud that aren't on YouTube (or something like Grooveshark).

It's earned a coveted spot in one of my always-open tabs. Combined with Designers.mx, it has completely displaced YouTube and Grooveshark, the latter of which I've always felt guilty about because I don't know how it's legal. And recently, it's even taking over Designers.mx as the player of choice. I love how I can just go to my favorites, hit play, and it goes through the whole list, even turning the page for me when it reaches the end of one. I don't know how many times I've just let it play and hours would go by without me noticing or having to think about changing the music since I'm so picky about what I put in there. It's perfect.

I've also grown quite fond of mixtapes because of SoundCloud. I never appreciated how much time and effort people put into the order of an album. With mixtapes, you're almost forced to listen to it from beginning to end unless you're okay with clicking randomly around trying to find a particular song. This is a completely different feeling from an album or even something like Designers.mx since I still find myself skipping around the songs if I'm given the choice. My favorite thing about them is that you only have to find someone you trust, someone you know who makes good songs, and you know there will be new songs and artists in their mixes that you've never heard before that you're going to love. It's amazing.

Even if I could already listen to any song I could possibly want to with things like Spotify and Grooveshark, I would only go as far as the music I knew and loved. Designers.mx showed me the joy of discovering new music, but SoundCloud makes that pool of music I haven't discovered yet many magnitudes larger, so much so that I haven't felt this excited about music in a long time.
