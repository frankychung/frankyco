---
layout: article
title: Little bets
slug: little-bets
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: habits
category: article
---
I stopped smoking for six months. It all started with a little bet.

I've been convinced for a while that the best way to get yourself to stop, or start, doing something is to make a bet with yourself. To put something on the line. To try to do this thing you want to do differently every day, and to make the bet that you can. As the number of days grows, what you're putting on the line is having that number reset to zero.

But this non-trivial accomplishment had another element to it. I made that bet with a friend, and we *both* put something on the line. It was money, not an insignificant amount but not an amount we wouldn't mind parting with for a good cause. 

I didn't really care about the money from day one. Not even losing it, which probably feels much worse than winning it. It was more that I actually cared that this person was offering something of hers for my sake. I mean, on the surface, we were laughing that if I fail, she wins, but really, we both win so much more if I make it out the other side. 

I'm curious how this might work the other way around. I watch so many of my friends fail at losing weight, exercising, whatever they say they want to do. It's almost more sad than the million things I fail at myself. So I think the next time the topic comes up with someone about something that I would genuinely be happy about him accomplishing, I might play the same game for him. 

So I won that bet, I mean I totally killed it, but I've never asked for the prize money. What she inadvertedly gave me is worth too much for me to ever repay back.
