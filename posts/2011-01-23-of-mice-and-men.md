---
layout: article
title: Of Mice and Men
slug: of-mice-and-men
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: tablet apple
category: article
---
I love experimenting with different gadgets for my computer. When something new comes out and the more I hear about it from reviews and blogs and whatnot, the more I want to walk straight to the nearest electronics store (and more often than not, the Apple store) with credit card in hand. If I manage to shut it out of my life I eventually get over it, like if I'm able to disconnect from the internet for a few days in a row to get it out of my system. That's clearly impossible and has never happened.

This is a story about a six month period where I transitioned from a regular Mighty Mouse to a Magic Mouse, barely managing to skip that Magic Trackpad and ending up with a Wacom Intuos4. 

The Mighty Mouse was alright. The little ball in the middle was cool but I could never get a reliable right click out of it. I didn't imagine that there was anything better out there though so I got used to it. I'm sure if all the other cool stuff didn't exist I'd be perfectly happy with it today, almost 2 years later. 

The Magic Mouse instantly clicked with me. The prospect of never having to actually click a mouse anymore was too tempting. Scrolling down a page just touching a slab of glass and plastic was way too cool. 

It turned out to be better than I imagined. Once I could click and drag by tapping without actually clicking, I felt like I was experiencing the future. Unfortunately I couldn't go clickless due to a few things like vector work which was slightly disappointing but it didn't ruin the novelty for me.

Then around last May or June I started to read an unusual lot about people using tablets as a replacement for their mouse. Not just artists and whatnot, but people that do the exact same thing I do! My interest was definitely piqued. I didn't quite understand how a tablet really worked but I knew two people in real life who make insanely awesome artwork for which they have praised the tablet for years. I also read on a web comic a few years ago where the author was praising the tablet, saying it is just like writing with a pencil on paper. I was skeptical at the time but with all the crazy things going on these days (iPad), I supposed anything was possible.

I didn't even bother to go out and test it (which is dumb, since a quick visit to Best Buy would have sufficed). I just ordered the smallest refurbished version, which was still twice as much as I would ever pay for a mouse (including the Magic one). 

Someone said it takes a little while to get going with it. You need to suck it up and use it exclusively for a week, resisting every urge to reach for your mouse or trackpad, before you realize its amazing potential. Well, I didn't go through any of that. It was love at first sight. This thing was packaged almost as nicely as my MacBook Pro (almost) and looks so sleek and fun to use. I picked up the pen and did a few swipey motions and I was hooked! 

It's everything I could have wished for when interacting with the screen. It is the natural extension of my hand that I thought the mouse already was. I love picking up the pen and where I touch on the tablet, it automatically transports the mouse to that spot in relation to the screen. I love hovering ever so slightly above the tablet to move the mouse around. I love dragging and resizing windows, oddly enough. Oh, and now I can draw! I'm not saying I'm an artist or anything, and there are plenty of ways to get your inspired creations from paper to inside your computer, but really, drawing directly into the computer is an amazing thing. It actually feels like drawing with a pencil on paper.
