---
layout: article
title: Starting over
slug: starting-over
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work programming
category: article
---

Have you ever thrown away something you've worked on for months? How
about a year? If you haven't, let me tell you &mdash; it's fucking scary.
But every time I've done it, it always turns out to be the best
decision I've made in a long time.

It starts with a nagging. You've got your head down, working hard,
moving fast, having fun, but there's something telling you, isn't there
a better way to do this thing you're doing? Yes, of course there is,
there always is. But you ignore it at first. Your way is awesome and
cool too. You think. Then it gets worse. It turns from doubt to certain
doubt. It turns from a nagging to a clear voice telling you, loudly,
that you're doing it *wrong*. 

And that's good. Because you're listening. Isn't that so much better
than being oblivious? Than not letting your ego get the best of you?
	
Every time I've started over on something, it becomes so many magnitudes
times better than what it was before. Which is weird, because you'd
think, why didn't you just do it this way from the beginning? That's
a terrible way to think. There are so many factors that led you to where
you are now. Imagine how far behind you'd be if you'd just waited for
the magical moment that you'd know what to do. You'd never
start. So feel *good* you're starting over, that you've found a good
enough reason, a big enough reason, to scrap it and start again.
Something so convincing, and something so exciting, that you're actually
revved up about the incredible amount of work, and learning, ahead of
you.

How does it happen? It's only happened to me two ways. The first is when
someone, or multiple someones, people who knows what they're doing and
has gone through what you're going through before, flat out tell me I'm
doing it wrong, or I'm doing it dangerously, or I'm doing it stupidly,
whatever. It hurts, but maybe it validates some suspicion you've had, or
it's some out of the blue amazing advice. I've gotten so, so, so much
better at what I'm doing thanks to these wonderful people. It's happened
many times. To the point that now I can see people doing it wrong, as in
doing the same exact things I was doing, and I just want to grab them
and shake them up and down and tell them things that are so obvious
now.

The other way starts from something smaller. Maybe you read a book,
or maybe even something as small as an article on a blog, and it piques
your curiosity to look at another tool or technique. And it quickly
snowballs into something that you clearly should be doing. And you spend
your nights dreaming about how you wish you could be doing it this or
that way. And sometimes, that feeling becomes so strong, and you become
so pissed at what you're doing now, that you go for it.

We scrapped something we were building for almost a year to rebuild
it again in a month, with an unbelievable increase in performance and
quality and somehow me becoming a much smarter developer along the way.
But it can be something smaller, like eating differently, or remodeling
your apartment. And it can be a lot bigger. Changing careers. Moving
to another country. It may feel like you're starting from scratch, but
you're not really starting over. You've just leveled up.
