---
layout: article
title: A lack of control
slug: a-lack-of-control
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work personaldevelopment 
category: article
---

I can ride a bike, but I've never been any good at it. I've never
figured out how to ride without holding the handle bars, for example.
And it's become quite apparent I don't have a talent for it whatsoever
now that I'm living in a place where riding a bike is extremely
convenient to get around with (meaning, it's not very practical to have
a car due to the close proximity of things and the prevalence of public
transportation).

Every time I have to weave through a group of people, I always come
close to grazing someone. I actually consider it a miracle it hasn't
happened yet. When I'm walking on the street, I'm amazed at how
practically everyone I see riding a bike, from a child to a mother
carrying her baby to those speedsters on road bikes, can do it so
effortlessly.

I think I understand the source of my discomfort when riding a bike. It's
my lack of control. I mean, I have it to a large degree, but it's that
little buffer of wobble you have to account for. And once in a while,
you bump or even fall a little bit outside that buffer, and when there
are people around, your imagination starts to get the better of you and
you worry. When I'm walking, I have precise control over the position
of my body at any time, and when that sense of control is gone when I'm
riding a bike, well, I freak out.

But when you stop to think about it, this is the same with everything.
First of all, you can always get better at it. I could practice riding
a bike every day, and I could shave that level of unpredictability
down to very low, but it's still there. It's like playing tennis. In
the beginning, you can hardly keep the ball in the court, but you get
better and better and eventually you might even be able to hit the
line consistently. But there's still that microscopic lack of control.
Same with playing the violin. It feels so awkward to hold the thing at
first. After years and years and it becomes second nature. But you can
feel there's always that space you have yet to move into, that room to
improve.

That lack of control, that space, is part of the fun, and part of the
key to doing it well. To learn to roll with it. I think about my work
sometimes, and how I am getting better and better at what I'm doing every
day, and how much I love that, but at the same time I think about how I
have an almost infinite amount of space to get better, and how, putting
it in perspective, I almost don't know what I'm doing, but that's what's
exciting. To give in to that lack of control, and let something else in
you take the steering wheel, to react to the stuff in the future you
can't predict.

I think that's why I haven't actually hit anyone with my bike, even
though I lack the confidence to confidently go through a crowd of
people. When my heart skips a beat because I get close, I let my body
get myself out of the split-second sticky situation. And when I see
these people on the street here every day blow by me by inches, I wonder
if they're doing the same thing. I see them wobble the same amount as
me. They might just be used to it, trusting themself to get themselves
out of whatever momentary sticky situation they're in.

