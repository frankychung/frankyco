---
layout: article
title: Artist's date, part one
slug: artists-date-part-one
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life personaldevelopment productivity creativity
category: article
---
I love [morning words](http://franky.co/2012/05/morning-words). But the second big to-do from The Artist's Way still deludes me. I find it difficult to go off somewhere for a few hours on my own. It's almost like I'm scared. Scared that I'm ditching work, even on a Saturday or a Sunday. Scared that I'm going to get lost, or that I'll be out of place. You know, the usual stuff to not be scared about but you are anyway.

It hit me hard one Saturday. Going to the museum is a bit of a cliche for an artist's date but this particular exhibit was intriguing. Plus someone I admire quite a bit recommended it to me, and I've never wandered around a gallery by myself, so I thought why the hell not. I woke up early, ate breakfast, and slipped out. I didn't have to be quiet about it but it was more fun that way.

Half the experience was getting there. Finding a new place by yourself is always fun, especially if you get a little lost along the way. It was a Sunday morning, so it was peaceful on the streets, like the city was still recovering from a crazy party the night before (and in Roppongi, there were no doubt countless parties the night before). 

The exhibit was about new media, so there was film, music, animation, comics, experimental stuff, computer stuff, all my kind of stuff. There were so, so many people, because it was the weekend and it was the last day of the exhibit (and it was free). I was there for two hours. At some point there were so many people I could hardly get through the crowd. But it was so fun going as slow as possible, absorbing as much from each booth, and hunting down the stuff that resonated with me.

There was something about being around so many strangers. The more people there were, the more anonymous I felt. This meant I was less self conscious and I felt free to enjoy everything around me. Big crowds usually tire me out but it was nice in this case. 

I found a film that made the whole trip worth it. It was a beautiful ten minute short with great music. It was playing on a tiny screen, yet it pulled me and the small crowd that gathered in. Even with the huge volume of chatter in the hall, this video was the only thing we could see and hear. Is that the point of these dates with yourself? To go out and find something like this? To be moved at least once by something beautiful and unexpected, after taking the time to explore? If so, I'm already a fan.

This experiment was a success. It's trying to recharge and reignite whatever in your creative self had dulled in the past week, or however long it's been since your last solo outing, and after taking this trip and not having to talk to anyone, I felt exactly that.

The thing is, for so long after that day, I haven't gone off on my own like that, not even once, even though I know how good it is. Half the reason is because I forget about the whole idea because I'm absorbed in other things, but the other half is because I'm still scared. 

I actually had a great idea for my next outing. A friend told me about at a certain used bookstore with a huge English section. One of her favorite things to do is to go there for an hour, browse, and bring a random book home, especially because it's so cheap and the selection is eclectic. That sounds perfect.

So why is this called part one? Because I need to revisit the topic when I finally get it down as a habit. Something I do every week, or whatever period of time turns out to work the best. Just like I write every day in the morning and how that's changed my life, I hope I can report back the same for this.
