---
layout: article
title: What's in a name?
slug: what-s-in-a-name
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: twitter
category: article
---
I take extra care in choosing my username when I sign up for a new app. I do it so much now that I've created a list in my head of aliases that I try from the top until I manage to get one that's open. I get a bit giddy when I put in just my first name and the form goes through. Anything /franky is just beautiful (franky.something is okay too). 'frank' is almost as good, although there's something cold about it. I don't like seeing my last name very much but sometimes it has to do. Then there's my [Twitter handle](http://twitter.com/ffffranky) which is impossible to explain out loud but I can't bring myself to change it.

So I started to wonder, what goes on in other people's heads at that pivotal choose-your-name moment? I've always thought the word a person chooses to represent themselves might tell you a bit about what kind of person he or she is. 

There are two major categories of usernames. The first are those that use some variation of their name. Our name works and looks fine, so why bother coming up with a new one? Sounds practical. You don't have any attachment to these kinds of things and want to get to using whatever you signed up for already. You see a lot of simple firstname + lastname combinations here. And there's nothing wrong with that. 

On the other hand, you have those that arrange the letters in their names into something distinct and beautiful. If you find someone that makes up a new word, yet it's recognizable as their name or some variation of it, you know they spent some time thinking about it. You're talking about someone who might pay a little more attention to details, to visuals, maybe a creative type. 

Now, if you find someone on Twitter or Facebook with a generic first name as their handle, or a really short three letter thing like their initials, you know they're someone who's always ahead of the game. They've probably snatched up that same generic name on a ton of other services because they're hip on all the new stuff. Not that this says anything about how hip they are in general.

The other kind of username is the one that doesn't have anything to do with the person's real name. I like these because there has to be a story behind it. Maybe it was a name they chose in middle school and it just stuck. Maybe they traveled around the world and found this word that means something profound in an ancient tribal language. Or maybe they felt a connection with a favorite character from a book they love. Find that story and you'll know the person that much better.

But, besides a very few exceptions, the only ones I don't get are the usernames with numbers tacked on the end of it. It might just be an extreme version of the I-don't-care variety above. If that sounds like you, come on, you can do better!
