---
layout: article
title: Counterintuitive
slug: counterintuitive
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: sleep softwaredevelopment bugs
category: article
---

Life's too short not to get good sleep. How many times have you stayed
up a couple extra hours to get that thing done, or to watch that thing,
or to not do particularly anything. Granted, there are many, many great
reasons to stay up late, but if you're in your daily routine, and you're
doing great work, usually, you should go to sleep at a reasonable time. 

Because most of us have a time they need to wake up by. You may be super
lucky like me and not have anything etched in stone by someone else,
but, like me, there's a difference between getting up at 8 and getting
up at 11. And there's a hell of a difference between 6 and 11. That's
more than half a day's work blown by (don't worry, I'm not encouraging
you to start working at 6). You could love the quiet of the morning, or
you're a night owl and get up past lunch time, whatever the case, to
make use of the day at all, you need to get good sleep.

You know those people who love to boast about how little sleep they
get? Everyone who knows what it's like, how important it is, to sleep
snickers. Not to say anything about them and their achievements and
their happiness and how awesome they are, but they're *missing out*
on the joys of a good night's sleep. That energy, that kick in your
step, that feeling of refreshment when you wake up, that feeling of
*good* exhaustion when you pull the covers over and you fall asleep in
*minutes*.

I still remember the days in school, when I would be falling asleep in
class, or walking dazily to class, or trying to do my homework or study
for the exam, and my eyelids felt so heavy and my head was dropping off
side to side. I learned and achieved little to nothing. I can write that
time off completely. But, an extra 1 or 2 hours of *good* sleep would
have made that time a different story.

This can sound like a stretch, but it's almost the same as technical
debt in a software project. If you have too much, and it comes from
moving too fast for too long, you're going to get stuck. You're going
to be moving a lot, lot slower (and in some cases, not at all if you
get far enough behind) than you *could* because you decided to take too
many shortcuts. 

Sleep debt is a real thing. It doesn't take just one night to correct a
week, a month, a lifetime of bad sleep. Hours don't add up like that,
just like a certain number of hours in front of a computer doesn't
correlate to getting good software written if the project is in a bad
state.

Now I say all these things, but like I said, there are so many reasons
to stay up late. Treat it as inevitable, and you can learn to plan for
it and manage it. Just like in any software, there are going to be bugs.
Things you overlook. When you accept that's the life of that line of
work, you're going to be okay.

I'm pretty sure there are times when we all feel stuck. We might not
know exactly what we're feeling stuck about, it's just that feeling, and
it could last days, weeks, *months*. I'm not saying sleep is *the*
solution, but if you're not getting all you need, then sleep could be
a key component in getting you out of that rut.

