---
layout: article
title: An attempt at explaining how to use Vim
slug: an-attempt-at-explaining-how-to-use-vim
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: vim
category: article
---
Vim has a tough learning curve. When you open it for the first time, you will be frustrated that you can't type a single letter on the screen, unless of course one of the words you type contains the letter "i". But also know this. When you finally get over that hurdle, you will be rewarded with an experience you never imagined was possible working with text. The thing that kept me going the first day or two was just reading the mounds and mounds of love and praise for the thing. In fact, [here](http://www.viemu.com/a-why-vi-vim.html) [are](http://stackoverflow.com/questions/597077/is-learning-vim-worth-the-effort) [a few](http://robots.thoughtbot.com/post/13164810557/the-vim-learning-curve-is-a-myth). And you will get it. It's not as hard as you think. Things will ingrain themselves in your brain. Just think of that sweet, sweet freedom of never having to touch the mouse again. Well, the benefits of not using your mouse might not be clear until you start rolling without it. Anyway, just give it a chance.

I'm on a Mac so I use [MacVim](http://code.google.com/p/macvim/). The rest of this post will assume you're on a Mac, although other OSes shouldn't be hard to figure out.

Starting out and understanding modes
--

The first thing I suggest you do is go through the Vim tutorial. Fire up Terminal and type "vimtutor". It will teach you the basics of moving around a text document and you can finish it in about twenty minutes. I'll be exploring and reiterating the points from the tutorial for the next few paragraphs.

The big philosophy behind Vim is that text editing and text navigation are two different things. You will switch between the two modes often, but you want to be spending most of your time in normal mode and only going into insert mode for short bursts of text. Understanding this concept is what allows you to fly around the text document with just your keyboard.

To insert text, you need to go to **insert mode** by pressing <span class="kb">i</span>. Whatever you type after you go into insert mode will show on the screen. To exit insert mode, press the <span class="kb">ESC</span> key. You can also use <span class="kb">CTRL</span> + <span class="kb">[</span> which is easier to press since <span class="kb">ESC</span> is a little bit out of the way.


Moving around the document in big and small steps
--

So how do you move around the text document? There are too many ways! But I'll list the most common ones starting from very big movements to small. The first is using <span class="kb">/.</span> This finds the next occurence of your string. <span class="kb">?</span> does the same except finds the previous. If you happen to know the line number you're looking for, you can type the line number and <span class="kb">G</span> to go straight there.

Speaking of <span class="kb">G</span>, press that to go to the end of the document. <span class="kb">gg</span> to go right back up. Oh yeah, capitalization matters.

To move down a big block of text, press <span class="kb">CTRL</span> + <span class="kb">d</span>. Going up is <span class="kb">CTRL</span> + <span class="kb">u</span>. If you want to go down a full page, <span class="kb">CTRL</span> + <span class="kb">f</span>. <span class="kb">CTRL</span> + <span class="kb">b</span> to go back a full page. So there isn't really any sense to the key maps in terms of how they're related spatially on the keyboard, but the keys sort of map to the English language (Down, Up, Full, Back). But that's not always the case. It's like learning the vocabulary of a language.

Finally, you jump directly to the middle of the page relative to the window by pressing <span class="kb">M</span>. The very top with <span class="kb">H</span> and the last line on the current visible screen with <span class="kb">L</span>. Mind the capitalization. Pretty easy right? Middle, high, low. I use this one a lot!

Finally, when you're a few lines away, press <span class="kb">j</span> to move down and <span class="kb">k</span> to move your cursor up one line. This doesn't have any pneumonic shortcut to help you remember it but it's one of the more general up and down keys in other applications.



Moving around on a line of text
--

There's a lot you can do once you're on the right line. I usually press <span class="kb">zz</span> to center the screen on that line if I'm going to spend a good amount of time on it (ie. more than a few seconds). I didn't know that one for a while.

If you need to start a new line, you can press <span class="kb">o</span> and you will automatically be on the next line *in insert mode* so you can start typing away. Vim will usually automatically put you in the right indention if you're inside a block. Type <span class="kb">O</span> (capital o) to do the same except start at the line above. I also do this when I'm formatting text and I need to set one line of buffer. As far as I know there isn't a default key to insert an empty line without going into insert mode but you get used to it pretty quickly.

Press <span class="kb">f</span> and any key and it will jump to the next occurrence of that character *on that line*. <span class="kb">F</span> to get the previous occurrence. This is really handy when you got your eye on a particular spot in the line. To get to the end of the line, press <span class="kb">$</span>. The beginning of the line is <span class="kb">0</span>. It just makes so much sense!

Next is one of the most important series of mappings. Press <span class="kb">w</span> to go to the beginning of the next word and <span class="kb">b</span> to go to the beginning of the previous word. Press <span class="kb">e</span> to go to the end of the current word. These alone will help you fly through a line. Press <span class="kb">W</span> to go to the beginning of the next word that is after a space. Same with <span class="kb">B</span>. These are also important commands for copying and deleting which we'll get to later.

To move right one character, press <span class="kb">l</span>. Left one character is <span class="kb">h</span>. You've got all directions covered with your right hand home keys! You'll start to wish every application used them instead of the arrow keys. Check out [ffffound](http://ffffound.com) for a cool implementation on a website (press j to go to the next picture!). Gmail also uses similar key mappings if you turn on keyboard shortcuts.


Inserting and replacing text
--

When your cursor's in place, time for some magic to happen. If you're staring at a blank block of space, you can simply press <span class="kb">i</span> and start typing away. When you're done, press <span class="kb">CTRL</span> + <span class="kb">[</span> right away!

Now what if you're in the middle of a line of text? If you're trying to insert something, <span class="kb">i</span> will put the cursor *before* the current character you're on, while pressing <span class="kb">a</span> will out the cursor *after* the current character. This is more important than you think! If you make a mistake, which you probably will the first few days, you have to exit insert mode, press a key, and go back into insert mode.

You can do some delicate work on the character level in terms of deleting and replacing too. To delete the character your cursor is on, press <span class="kb">x</span>. To replace a character, and this one actually comes up a lot, press <span class="kb">r</span>, the new character, and it will automatically take you back to normal mode.


Copying and deleting
--

Now for copying and deleting text that's more than one character. This is crucial. It feels unusually good when you can effortlessly copy and delete whole chunks of text and fling thing them around the document with precision. The key to remember for copying is <span class="kb">y</span>. It stands for yank. I rather like the term. But pressing <span class="kb">y</span> by itself doesn't do anything. You need to either select a block of text in visual mode or combine it with other commands. 

Press <span class="kb">v</span> to go into visual mode. Now as you move your cursor, it will select all the text between your cursor and the point you entered visual mode. When you are done selecting the chunk of text you're looking for, you can yank or delete it. This is like selecting a block of text with your mouse except much more powerful because you all that power of flying around your document. Let's try it! Select some text, press <span class="kb">y</span>, then press <span class="kb">p</span> on a new line to paste it. Maybe "p" stands for put, but both are good words to remember.

Now we get to one of my favorite commands. If you press <span class="kb">dd</span>, it deletes the whole line. It also saves it so you can press <span class="kb">p</span> to paste that line anywhere else. <span class="kb">yy</span> will copy the line. It's awesome. 


Stacking and repeating commands
--

One of the most powerful things about Vim is the concept of combining or stacking commands. For our first examples, lets work on the word level. To delete one word, press <span class="kb">dw</span>. Two words is <span class="kb">d2w</span>. Copying three words is <span class="kb">y3w</span>. It's easy to remember since it flows like the English language. Yank three words. And this applies to all commands. Press <span class="kb">10j</span> to move your cursor down ten lines. After a while, adding a number to your actions will become second nature. The one I use the most? A number combined with <span class="kb">dd</span> to delete whole blocks of text and having it ready at my finger tips by pressing <span class="kb">p</span>. So awesome.

A lot of what you do in coding or text editing or whatever is a lot of repetition. There is a magic key for this: <span class="kb">.</span> - it will automatically repeat your last action.


It's only the beginning
--

That's a lot of stuff. And I use each one hundreds of times each day. Vim has become second nature and yet it's still so *fun*. Seriously, try it out. There's quite a bit to learn and get used to, but it's one of the best investments you'll ever make.
