---
layout: article
title: Happy Birthday
slug: happy-birthday
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: apple macbookpro
category: article
---
My MacBook Pro just turned one year old. It's been really good to me the last twelve months.

I think what really got me to pull the trigger on the thing was seeing it in the store and thinking, finally, good color on a laptop. I've had one laptop before and its color gamut was so bad it drove me insane. All my friends' MacBooks from the prime white and black generation were pretty bad too. I also knew plenty of people with the Pros and those were beautiful. I just didn't put it in my realm of possible purchases because I didn't feel like dropping two Gs when I already had something that worked "okay." 

Then the aluminum MacBook came out. Unfortunately it was another disappointment. I stuck with my three year old Sony monitor because it didn't wash everything out with a weird bluish hue. 

Clearly this doesn't matter to 99% of folks out there and that's quite alright. But the right color gives you the best experience for everything you do on the computer. Photos, movies, surfing the 'net, drawing, design, even writing. Okay, so listening to music doesn't matter that much, but honestly, I feel a lot more comfortable scrolling through iTunes on my machine than I do on my friend's Asus.

And now some other things I love about it, off the top of my head.

It looks good. Does a laptop get simpler than this form? My friend has an HP and once in awhile I'm forced to use it to do something or another on Windows. Every time I see all those stickers and weird buttons and flashing lights, I shake my head in pity. How does he put up with the thing?

It's quiet. I remember the first computer I built. It was deafening, even tucked under my desk. My next laptop wasn't any better. Any strain at all and the fans went totally out of control. I'm not even sure my thing has fans. If it does, they whisper quietly enough that I don't know what they sound like. It's insane how much of a difference it makes when the only sound you can hear is yourself typing on your keyboard.

It's loud too. Surprisingly, the speaker volume can get pretty high. Of course the sound quality isn't something to rave about, but it's totally sufficient for watching silly YouTube videos, listening to music casually as you work (or don't work), even watching movies. The other night we found ourselves without a sound system at a party and it was more than adequate in a pinch. 

It's the perfect size. I love aiming to do the most with the least amount possible. I had an iMac but I always felt silly on the big screen when I always only have one window open at a time (thank you, SingleAppMode). It fits in my bags. It's not too heavy (not that it's light). I'm really curious how well I can do my work on the smallest Air but that's probably pushing it.

It's got a solid build. Obviously it won't come out unscathed if you drop it but as long as you're careful and clean, the thing will look pristine for a long time. When I close the lid, I have no qualms carrying it around with me as is.

There you have it, the perfect computing machine. Already out since 2009. I mean, at least until a more perfect machine inevitably comes out.
