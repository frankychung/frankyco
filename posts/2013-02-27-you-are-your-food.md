---
layout: article
title: You are your food
slug: you-are-your-food
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: food
category: article
---

The other day at dinner, a friend told me people are what they eat. Now
I can't see unsee it. All of us each have our unique personalities,
charming in our own ways. This friend of mine especially. He's a deep
thinker. He's an artist. A creator. And this comes out completely in
the Instagrams of his food. Simple, elegant, healthy, light. I look at
his work, and that's what I see. Underneath it, there's this subtle
complexity, of thought, of stuff only he knows about but makes the whole
thing more cohesive, giving the whole thing a bigger point than the
individual parts. I'm talking about his food and his work.

I look at another friend. She's an amazing cook. Her food is bursting
with flavor and is beautiful to look at. You don't meet many people who
put so much effort into the *presentation* of the food. I know I don't.
But this is just like her and her work. She will do the work, but do
the work so well, and consistently surprise you with the quality of her
ideas.

Then I have my friends who don't cook at all. Some of these friends seem
like live life by the minute. They work hard and they work fast and are
very spontaneous. Makes sense, considering they have a million things
going on at any given time. So when they're hungry, they go out and buy
fast food or eat out at a restaurant. It's not that they're *lazy*, it's
just not in their *plans* to make their own food. So it's more
interesting to observe *what* they order out when they do that speaks to
what kind of person they are.

There are the people who eat out often but still order the
light, healthy dishes. There are those that eat the most hearty,
guilty-pleasure ones. I think the first one takes a more peculiar person
than the second, since I'm pretty sure it's always easier to order
the fried chicken over the salad. But even more complex is *how much*
someone eats in a sitting. There are those who will always clean up
their plate. There are those who stop the instant they get the thought
that they might be full. Now add in what someone orders for a drink.
Beer? Wine? Cocktail? Whiskey?

Another thing about those who always eat out or not being picky about
what they eat is it seems they don't have too many *preferences* in
general. It's like the people who stick with the first thing they see or
what's put in front of them, versus the people who are picky, in terms
of food, and other things like say, clothes, tools, work, toys.

And it's not just what you eat. It's how you eat it. There are the slow
eaters and fast eaters. Don't you think the slow eaters seem to have a
little bit more on their mind? The fast eaters seem to be the ones that
like to get to the talking. Both are awesome, of course.

So you can learn a lot about someone by their food habits: Do they bring
in lunch or eat out most of the time? What do they eat? How do they eat
it? How much?

Here's the really interesting part. It's what happens when people change
their food habits. I've seen some people, when they started eating
healthier, they began acting, living, speaking differently. I've seen
couch potatoes with no energy turn into bright eyed talkatives with a
spring in their step. I've seen reformed workaholics finally manage to
take a break once in a while. I've seen people start to *exercise* after
changing their diet.

What kind of impression are you giving off with the food you eat? What
kind of person do you want to be? Start eating like one. It could start
a chain reaction to affect every other part of your life.
