---
layout: article
title: Excuse my French
slug: excuse-my-french
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: cooking webdevelopment
category: article
---
I'm starting to think learning any sort of skill is like learning a new language. There's a similar thread running through learning how to cook, learning how to play the guitar, or even learning how to make a website.

People can get pretty gung-ho about learning a new language. I remember watching my friends at school struggle and hit so many walls but they refused to give up. I happen to speak another language at home and even though I'm not so great at it, people persisted with innocent questions with that look in their eyes of genuine wanting-to-know. But then these same people say they wish they could cook, or they don't understand computers and the internet, and I smile and nod, thinking there's something off about what they're saying but not being able to pinpoint why.

They might think things like cooking comes naturally to people and if they aren't instantly good at it, they should drop it and let someone else handle it. That's silly because it doesn't come naturally to anyone. I think people who are genuinely interested and take up the challenge to overcome all those intimidating walls are the ones that manage to learn anything worthwhile. It's not reading a cookbook, it's actually putting things in the oven and seeing how it turns out. It's those times when you make subtle adjustments to get it just right that gives you valuable insight on something. It's those times where you take the time to figure out what you did wrong that something gets cemented in your head. 

Of course, if you don't like to cook, there's nothing wrong with that.

It takes awhile. People who grow up speaking English are really, really good at it. That's years of training and practice. It's the same with people who can serenade you with their guitar. They've trained their brain in the same way they trained their thoughts and voice to think and speak in a particular language. 

I think you can break down learning a new, grandiose skill over time the same way you can break down the bits of language. First there's vocabulary. The core pieces of whatever you're trying to learn. So it's all the tags with HTML or all the properties you can apply to each element with CSS. You'll probably need a reference to pull out the ones that you want to use when you're starting out, just like you'll use a dictionary when you don't know any _words_ of a new language. The more you use them, the easier it is to pull them straight from your head. 

Next is grammar. How to put that vocabulary together. The rules and relationships of these words. It's like how an HTML document is structured and how to write semantic markup.

Finally, it's your technique and if you can build something with confidence using the knowledge that you've built over time. When you can swagger into a restaurant and order any item on the menu. When you can hold a gripping conversation with a stranger. When you can write a lovely poem or tell a riveting story. You're able to express exactly what you're trying to say with style.

I would like to think I'm somewhere between steps 2 and 3. Unfortunately I don't know how big that gap is. I think I can make a pretty good website, but to make something beautiful and moving… I've yet to get that hunch, looking at something I made, that this is something I absolutely love. Who knows if we ever get that feeling when we look at our own work. You know when you create something and you're like, meh, but you pick it up a few years later and you think to yourself, "I made this? I'm impressed."
