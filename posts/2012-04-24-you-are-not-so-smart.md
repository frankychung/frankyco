---
layout: article
title: You Are Not So Smart
slug: you-are-not-so-smart
image: ''
embed: ''
link_url: http://youarenotsosmart.com/the-book/
footnotes: ''
tags: psychology
category: article
---
I love reading about psychology and how our mind plays tricks on us. They're usually peppered across articles on the internet so finding this book was a joy. You've probably heard some of the things in this book before but you're also bound to find a ton of new stuff that will blow you away.

The tone of the writing is down to earth and often funny, which I didn't expect because books like these always seem to take on a serious tone. I assumed the author was a scientist or a researcher, but he's actually a journalist who started the whole thing as a blog, to get back into this kind of writing and to share his interest in how we are not so smart. That's lovely because the facts and explanations are presented in a way that's easy to understand for you and me.

I couldn't catch any sort of order in the chapters. Each one is contained within itself and is perfect for picking up and consuming quickly, like posts on his blog. After he explains a concept and lays out the facts, he usually ends the chapter by offering a nugget of wisdom. There was something so reassuring about this. It makes you feel everything he included in this book can be helpful to you in some way. Several of the topics really stood out because they have some significance in my life, which I'll highlight now.

My favorite was the chapter on how we only see a fraction of what's in front of us. I've always been vaguely aware of this phenomenon but the implications never hit me until I read it here. It takes significant effort to keep concentrated on everything in your peripheral vision. The day I read this chapter, I tried a new way of looking at everything on my way to the office. Just that small shift of widening my perspective changed how the whole world looked to me. I've been here (Tokyo) for a year but it only hit me on this day how chaotic everything here is and how it's actually kind of beautiful. Plus it helps to use that energy required to concentrate on these kinds of things to help me get out of having listen to myself think all day.

Another one that grabbed me was how we can't really explain why we like things, and if we try to, it's probably bullshit. It explains so many things that never made sense until now. For example, for some reason, I've recently become enamored with eighties style synth pop. Like, borderline obsessed with it. And I have a few friends who think it's weird. I think it's weird too, but in the best way possible. So they always ask me why I like it, and I could never come up with a good answer. Nothing specific comes to mind. I just... like it.

I ask that kind of question a lot too. When one of my friends is addicted to the most horrible music or whatever, I always ask them why, expecting an answer based on logic, and getting an entirely unsatisfactory answer. Now I know it's okay. I don't have to know because I can take comfort knowing they don't know either.

And even when I do get a logical reply, something always seems off about it. They think about it and come up with this cold, calculating answer. Now I know I can take these answers with a grain of salt, knowing that it's most likely bullshit, even if they don't know it is. It's like I discovered a secret that will help me navigate the world just a little bit better.

Here's another one I loved. Venting doesn't help when you're angry. It only makes it worse, actually. That blew my mind. I don't know if it's just me or if I'm really in a high stress job (what do you think, other start-uppers?), but I've had a fair share of blood boiling in the past few months. I try not to let it out, although I have a few friends who always ask me how I am and I just blurt it out. And they listen. It's a great feeling knowing you have people who will listen. But it certainly doesn't make me less angry. I would thank them for letting me vent, but I wouldn't feel any better. Thankfully, it turns out time cools you off just fine.

There's one more chapter I have to mention. Apparently, certain words can make your heart beat faster, as well as make you nervous or angry or some other intense emotion. The moment I read this it reminded me of all the times email has done this to me. Like, when I get a nasty email, or when I was waiting back interview results in college. I distinctly remember certain emails that made me feel like my heart was going to jump out of my chest. And the thing is, I've had a friend say the same thing happens to her, and I thought, it isn't just me? I was sort of relieved but still had doubts, since I wouldn't say this friend and I are exactly normal. I guess now that I know it happens to everyone, I can start to look out for it, and even though I probably can't stop it, the emotions I mean, I can stop feeling so shitty every time it happens. Which is good, because I hate email so much, and I don't want to give it the satisfaction of giving me a heart attack down the road.

There's so much good stuff like this. I'm sure you'll find some that will resonate with you like the ones did above for me. And if even one changes your life, and I'm sure at least a few will, then that's worth reading this book a thousand times over.
