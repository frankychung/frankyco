---
layout: article
title: Recommendations
slug: recommendations
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: friendship
category: article
---

So many decisions in our life are based on recommendations. A lot of
them make sense. The food you eat, the things you buy, maybe big things
like where you live and where you went to school, none of that was
picked out of thin air. There's too many choices and too many unknowns
to juggle, so others's opinions are invaluable for your sanity.

There are a lot of people who have no idea who you are who influence
you. Maybe it's a famous chef, or your favorite movie maker, or your
favorite author or your favorite singer-songwriter. What is your job? Do
you have someone to look up to? Don't you get intensely curious about
what tools they use, what books they read, how they got to where they
are? Use them to guide you among the overwhelming number of choices you
have about everything.

So these people you don't know, it's easy to gather them and keep up
with them and listen to them. Unfortunately, it's difficult to talk
to them, ask them questions, which might be okay. The end decision is
yours after all, so you're not forced to blindly follow them. But then
there are the people that you can talk to, your friends and family and
acquaintences. With the ones you don't know, you don't have to put up
with the ones you don't like. In the other group, you have to put on a
filter, to get past the noise.

You get better at it. You get better at spotting someone who's in sync
with your interests, with what fascinates and bores you. With what
disgusts you and makes you angry, with what makes you laugh and what
makes you cry. You adjust accordingly, like this scale that lives in
your head subconsciously for everyone you know, and as each successful
recommendation registers, their words weigh heavier, their opinion sways
you more, and your respect for them grows. The scale moves in the other
direction too, so for some people, you know to take their words lightly,
or to even avoid them as much as possible, or at least tune out most of
what they're saying.

The interesting thing is, you would think that because some people know
more about certain things than others, you can't really judge people by
the whole person and you'd need to consider the topic of discussion.
This isn't the case. There's hardly ever one expert on one subject. If
you find someone who's supposed to know but who's constantly spouting
crap, don't bother and find another expert that you can actually stand,
someone you get along with.

Think about your best friends. How much have they influenced you? And if
you don't like someone's recommendations, and you consistently don't like
most of what they're talking about, aren't you a bit wary of them? It's not
that he's a terrible person, but you're not right for each other. You
need help navigating this congested world and finding the right people
is key.

Think about when you recommend something to someone. It feels good
because someone asked for your opinion in the first place. Or when
someone starts using something that you use, even if they didn't ask
you, but you know it was because of you, and you smile because you know
you got to play a hand in that decision. You become a little closer to
them. And think about how sometimes after you recommend something, they
come back and tell you they didn't like it. That's fine too, nothing
wrong with that. You know not to give a recommendation as lightly to
this them next time. It goes both ways.

If you find that person, someone who you go back and forth with,
finding that you love all the same things, and as a result of both your
experiences and outside recommendations together you make up an infinite
pool of new and fascinating things, *don't let that person go*. Because
I bet at least once, and hopefully that's it, and maybe it's already
happened, you'll lose someone like this. Maybe you naturally drifted
apart, or maybe you broke up. But think about all that crap she shared
with you, and think about all the stuff that she might have shared with
you over the years that you might never find out, and think about all
the stuff that you want to share with her because you know she will just
love it but she might never find out.
