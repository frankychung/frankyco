---
layout: article
title: The good enoughs
slug: the-good-enoughs
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: productivity creativity work
category: article
---
Is good enough a good or bad thing?

I used to think it was the best thing in the world. It was a great guideline for when it was time to move on to the next thing on my list. I'm talking about all my lists, like my list of bugs for the current round of revisions, or my list of weekend projects, or even my list of long-term goals.

But something irked me about having so many good enoughs lying around. I got addicted to finishing things without much thought about what I was finishing. So every once in a while, I would get the strong enough urge to take some of them further. It feels good to hold something that someone had put some heart into. It's hard to tell how much something will change when it goes from good enough to great. It could be just a few things that needed fixing, or it could be a complete 180, you can never tell beforehand.

What's painful is that it's impossible, simply impossible, to turn all those good enoughs into amazings. If you spend too much time revising, you'll run out of energy to create the good enoughs to turn around in the first place. A bit of a chicken and egg situation, I suppose.

So now we get into how wonderful good enoughs are, even if we've established that they can be dangerous. They keep you going. They're the fuel to the process. The motivation that comes from creating and finishing. The more good enoughs you have, the more energy you have to turn some of them into greats.

Most things worth doing need a delicate balance. You can choose to have a portfolio full of masterpieces, but they won't exist without a pile of good enoughs to choose from. But move too fast and you won't have any good enough to put in your portfolio at all.

A peculiar thing happens as you get deeper into the act of creating whatever it is you create. What you considered good enough when you first started doesn't make the cut a few months down the road. What you consider good enough today looks a lot like what you have hanging on the wall from last year. And what you polish into great always seems to be the best work you've ever done. It's a wonderful thing to just trust the process sometimes.
