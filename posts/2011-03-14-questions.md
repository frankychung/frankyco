---
layout: article
title: Questions
slug: questions
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: quora stackoverflow
category: article
---
I love Google. It has the answers to everything in the world. Well, except for the stuff everyone wants to know about but isn't very tangible, like love and stuff. But still, for the other 99% of the things you want to know, it'll do the trick, and fast. How to do something, anything? A quick list of things to do for that new place you're visiting next week? The conversion rate from Celsius to Fahrenheit? (I use that one a lot.)

In fact, now I just type questions directly in the search box. I like how I don't have to be strategic with how I word it. Most of the time, someone has already asked the same question and most of the time again, Google has found it and packaged it up nicely.

Question &amp; answer sites are great. One I stumble into a lot is Yahoo Q&amp;A. People ask the most random things, and for some reason they answer the most random things too. I guess that's good for me, since I ask the occasional random question. Another one I run into often is [Metafilter](http://metafilter.com). The quality of the answers there seem a little better than Yahoo. 

Then there's the one that has a special place in my heart. [Stack Overflow](http://stackoverflow.com) has created the best encyclopedia for anything I could ever think of asking related to building things for the web. With the exception of one particular problem, everything that I've ever hit a wall with had already been asked and answered. The site has shown me the right way to do so many things that I never imagined I would be able to do. It's awesome.

But my favorite Q&A site? [Quora](http://quora.com). I love Quora. I almost never participate in these social web things, except Facebook of course. But with Quora, I can't help but vote up questions I like, follow questions that interest me, follow topics that intrigue me, even ask and answer questions myself. It took maybe 10 minutes to get used to it. It's fast, unique, and fun. My favorite thing about it is finding questions that you remembered thinking about but never thought to ask, or questions on things you would never think to ask about or don't know enough about to ask but after knowing the answer to you have at least been intrigued. It's a Q&A site that you can browse. That makes it a dangerous [time suck](http://www.urbandictionary.com/define.php?term=time+suck), which speaks well to the quality of a site, in my opinion.
