---
layout: article
title: Novelty sticks
slug: novelty-sticks
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: ipad writing
category: article
---
I think typing on the iPad is wonderful. I did it for the novelty at first, but two years later I'm still at it, and still riding on how weird it is that I'm able to be productive at all on the thing.

What I mean is, every time I fire up [Writer](http://iawriter.com), I'm struck by how I'm just poking different spots on a single slab of glass and, somehow, something comprehensible comes up on the screen. More so now that it's gotten to the point where I can type just as fast on said slab of glass as I do on a keyboard.

But it's gone on to surprise me with how practical it can be. For example, it's extremely portable. I mean, the iPhone is better for most things on the go, but you can't write on the thing. The iPad is perfect for typing on the long commute, or when the person in front of you in economy has understandably reclined all the way back.

And I like how you're almost forced to concentrate. There's a pretty big barrier to switching to another application, since it isn't just an alt-tab away. I'm not saying that having a small window and being semi-locked down will help you write better or write more, but once you're already sitting down and working on something, it could put you in a better mindset than if Facebook was open in another tab.

I can't remember when I started writing anything longer than just a few notes. I do remember it was on a whim, like, I wonder how this might feel kind of thing. I wasn't impressed but I wasn't turned off either, and it was certainly novel. I'd come back to it only once in a while, until Writer came out. It wasn't that the app was really cool (although it is what I use and have come to love). It's that someone even made an app for this specific reason that said something about the possibilities that I had only toyed with. Granted, you often see the app demoed with a bluetooth keyboard, but they don't put that extremely useful extra row above the onscreen keyboard for no reason.

Try it. You might find yourself with a useful writing tool you never knew you had.
