---
layout: article
title: The Hunger Games
slug: the-hunger-games
image: ''
embed: ''
link_url: http://www.amazon.com/Hunger-Games-Trilogy-Boxed-Set/dp/0545265355
footnotes: 1. <span id="fn120120221">It won't be hard to watch the movies though.
  Those are going to be great. Not sure how they're going to pull off PG-13 on that
  last movie.</span><a href="#fnr120120221" class="fn-return">&#8617;</a>
tags: thehungergames
category: article
---
I read through the three books in just as many days. It was exhausting, like I went through some major ordeal. I think that's the triumph of these books, that it pulls you into this insane war and doesn't let you down easy. It was a treat to read this from beginning to end without interruption. I'm going to try to talk in as much generalities as possible, and nothing you won't read on the jackets of the hardcovers, but still, if you have even an inkling of interest, go ahead and read the books. They're good.

I thought, even if it's a book about kids killing each other, it can't be that bad, since these are children's books (or young adult, whatever that means). That's the first thing I was wrong about. It gets pretty gruesome. Certainly not over the top, so it's not repulsive, and in a way it becomes a character in itself, something you become used to, a looming force that doesn't shy away from snatching any of the characters from right under you.

The second thing I was wrong about, and I was so suspect of, was that this is such a rip off of Battle Royale. How could she possibly not have heard of it? But it turns out they are so different, despite the corrupt government forces kids to battle each other to the death idea, that it's plausible she really never did hear about it, or she does a great job taking it in an entirely different direction. Like, how the kids know they're selected, that they're being filmed and televised, that there are those trained for it, that there are people controlling the environment, that it takes place in a futuristic setting.

I'm going to keep talking about [Battle Royale](http://en.wikipedia.org/wiki/Battle_Royale). Even if you've never read the book or watched the movie, or even heard of it, it'll give you a better idea of what The Hunger Games is like (and you just might want to pick up Battle Royale too because it's good). I think the biggest difference is that The Hunger Games is in the first person perspective of Katniss. So you read only what she sees and thinks in her head, and you're guessing about things around her as much as she is. Compare that to Battle Royale where you get an intimate look and background on many of the major characters. You sympathize and feel for them. Besides the characters Katniss interacts with, you get that feeling of desperation from her perspective instead of feeling it for the whole group. Both are great stories.

I imagine it must have been difficult not to make the love story cheesy given the circumstances. Battle Royale was hard to take seriously sometimes because it seemed like everyone had a crush on everyone and I still thought they did a decent job of it. I was pleasantly surprised with how it was handled here. There were some eye-rolling moments but I liked how the love triangle ends.

I set the last book down initially disappointed, almost mad, at how everything went down. The last huge push of action was craziness. You could imagine any number of scenarios of a happy ending and this would land far from any of them. But after letting it digest, I started to appreciate more and more the way it ended. I read that these books were inspired by what her father told her of war and it makes a lot more sense.

Katniss is a great character. Yes, she's brave and intelligent as you'd expect, but she's also brash, impulsive, emotional, scared, rude, and susceptible to what war does to people, how it changes you. It's how she manages to barely hold on after everything that happens to her that is thrilling to read.

I highly recommend it. You won't be able to stop reading it after the first chapter anyway. It gets right to the chase. I imagine Suzanne Collins had a lot of pushback on how everything turns out. After my withdrawal from finishing it all I decided to scour Goodreads and Amazon to see what others thought about it. It seems pretty divided. There are people downright angry with the last book. I understand what they're talking about. But then there are so many people that loved it. No punches were pulled. This is what war is. And thats why I've come to love it too. It will be hard to read again.<sup id="fnr120120221">[1](#fn120120221)</sup>
