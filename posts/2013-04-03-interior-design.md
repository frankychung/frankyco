---
layout: article
title: Interior design
slug: interior-design
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: design
category: article
---

I remember how it was exciting to start with a blank slate when I moved
into my new apartment. I spent hours looking through furniture and
interior design sites and magazines for ideas and inspiration. I day
dreamed about what couch to get, what fridge to buy, even what kind of
washing machine I'd get.

It wasn't daunting at first because I chose to live in a small place. It
was a simple goal. "I want to live here." What did I need to make this
place feel like home? I only had to fill two rooms so it seemed like the
perfect balance of not having to decide on too many things and being
able to make it right for me.

Then things got complicated. Every time I got a new essential thing for
the place, three more essential things I didn't realize I needed came
up. I got a refrigerator, so I could finally get grocieries. But where
do I put the food that goes in a pantry, when I don't have a pantry?
What spices do I need to buy to cook all this food? And shit, I don't
have any plates or utensils to eat this stuff with. Later I realized
I needed a microwave, a toaster, and I needed to fix my broken stove
exhaust. All things I took for granted or didn't know I needed until it
wasn't there. All new pain points that started from a simple goal.

This is what interface design is like. You start with the same
thing &mdash; a simple goal. Take a problem I don't think
anyone has completely solved yet (although some have gotten
[pretty](http://www.basecamp.com) [damn](http://www.asana.com)
[close](http://www.trello.com)): "I want to manage my tasks." You need a
checklist. No wait, you need multiple checklists. And you need to work
with other people. And you need reminders. And you need figure out how
to prioritize things. (That's only the beginning.)

And so you build a first version. Then you decide on your next feature.
You're thinking about how to make this new thing play well with what's
already there. Your first decisions are crucial because what you have
guides where you're going next, but you have no way of knowing how
exactly something will work out (although from what I've seen, this is
something that gets better with experience).

Designing is making compromises. You want to make everything as easy as
possible. But every time you make something easy, you make something
else hard. Or impossible. Or maybe when you make something easy, it
makes something else possible that you hadn't thought of, something
that might be insanely great, and you wrack your brain to think of ways
to make that new thing easy too. And eventually as your simple thing
sprouts into something bigger, that other thing that was easy before
inadvertedly gets harder to do. It's hard to keep this thing trimmed and
upright and in control as you water it more and more.

Take my ugly rug. After a while and a lot of fidgeting, it actually
doesn't look too bad. And it's actually serving a purpose I never
thought it would. It's so soft, I find myself laying on it half the
time I'm home. It's grown on me a lot. And through a series of small
purchases and gifts, my place is slowly filling out. And I finally got
the the right coffee table. Although it was different from what I was
imagining, I knew it would be great the moment I stumbled on it at the
store when I wasn't really looking for it.

So although design is a ton of compromises, it's also a lot of
improvisation. You end up surprising yourself. You stumble on things
you didn't see before. You find new ways to do old things, or you find
entirely new things to do. People give you great ideas, and you find
great ideas on your own.

My place finally feels like home. I look forward to sitting back on my
couch and reading a good book after a long day's work. That's how you
know you've designed something great. Not when everything is perfect and
every possible thing is easy and simple. It's when you feel at home.
When you look forward to using it.
