---
layout: article
title: Invisible
slug: invisible
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: design
category: article
---
 
All the work we do is invisible. I have no idea how anything works.

Take my car. I have a car mechanic friend who could open it up and fix whatever's wrong with it. He's spent years doing it and he loves it, and I love that he loves it and that I don't have to worry about.

I love music. I have no idea how people make the kind of music I love. Where do they start to come up with that melody? Those lyrics? How do they put all the pieces together? How does the band rehearse? And what about this song is making me love it?

I can say this about almost anything. I love movies. I love photography. I love this computer. And all of it is invisible to me. 

I know this because I build something invisible too. And I know it's invisible because it was just as invisible to me before I started to be able to see it. People look over my shoulder see  nonsense on my screen. Just like everything else, I couldn't comprehend how any of it worked, where things came from, what _things_ flow in what direction. And it's hard to process all the steps it took to uncover it. It's taken years and a million connected paths that led me to where I can build one of these things that I'm no longer clueless about.

The thing is, things become even more mysterious and wondrous as they become less invisible to you. You start to notice things. Things other people don't even know are there. You notice things that someone had put a lot care and time into and is why people love the thing, even though they don't know it. Take Facebook. Everyone loves Facebook, but I love it for different reasons. I find so many great touches in the interface. People spend hours scrolling through pictures and their news feed because these things are done *well*. I love the beautiful icons, the thoughtful layout, and I love that new Timeline. Plus there's all the shit I know is technically difficult that they nailed. 

I noticed this when I started to notice good writing when reading. I've recently been into writing. I try to write every day, and I try to make an effort to write well. To write and convey my thoughts concisely. So when you're reading a great book, or a great article, you don't notice how much effort these people have put into the writing. How hard it is, how much revision has got into it. Something well-written is a joy to read because of all the invisible work behind it. Sometimes I'll read a single sentence or paragraph and I'll be struck by how good it is. I've started to see the difficult technical decisions that had to be made when crafting a beautiful sentence. I see how good it is and think about how far I am from that skill level, which is exciting because I have so much to learn.

Seek out answers to the invisible. It's easy to ask the right questions if you take some time to think about it. How does that work? What does that do? How would I do this? Now's the best time to ask. Everyone's connected and information is so accessible that you're bound to find the answer. Chase these answers and soon you'll surprise yourself with what you can do. Whatever you choose though, is going to be the hardest thing you'll ever do, so picky about which *ones* you chase.

There are two situations when normal people can see a normally invisible thing. The first is when something is astonishgly good (or bad). This reminds me of Infinite Jest. That novel is so difficult to read yet so fun, and you know it fun is from the writing. The second is when you dabble in it. Like it's a pet thing of yours. You're not an expert, but you appreciate it more than others.

Find things that interest you and dig a little deeper. It's perfect fuel for your main, invisible work.
