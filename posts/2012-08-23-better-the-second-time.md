---
layout: article
title: Better the second time
slug: better-the-second-time
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work movies
category: article
---
You know what I love more than watching an amazing movie? Watching that same amazing movie a second time, and having that second time end up being better than the first. 

Most movies in my top tier of favorite movies pass this test. So much can happen in two hours. So many people put a year or two (or three) of their lives into making this two hour *thing*. I bet they wouldn't want any less from their top fans than to watch it more than once.

What is it about the second time around? It's all the things that you missed when you were enamored with whatever grabbed you the first time. And while you catch those new bits, you're still enjoying the things you love already. It's almost a new experience, even though you're watching the same thing. For example, imagine a scene with memorable dialogue. The second time, you already know what they're talking about, so you take in more of the details, such as the setting, the mood, the lighting, the music, the nuances of the actors's voices and postures. You saw these things the first time around, but this time you're able to relish them. And who knows, you might even catch something new from the things they're saying, even if you went into it not expecting any more surprises.

I've even seen certain movies three or four times. I don't think it gets better than the second time, although it can be *just as good* the third and fourth time... except when it's been a few years. A few years changes a person. I think you'll see things in a different light after five years, no matter if it's your second or tenth time watching something. The exciting thing is, it's almost never the case that you dislike something after a long time like that. Your tastes only expand. So you'll still love most movies you loved, but maybe you'll also start to love the movies you didn't like much or didn't quite get before. And that's amazing. 

Obviously this doesn't apply to just movies. I've listened to my favorite songs hundreds of times. I can't watch a movie a hundred times or I just might. Sitting down with an old book might be one of my favorite things ever. I don't do it often since there is an insane amount of new books to read, but it's still an adventure. The second time around, you're no longer in that page-turning mindset of needing to know what's coming next. You can take your sweet time to take in everything. Of course, as you get better at *reading*, just like you can get better at watching and listening, you can take in more of this everything I'm talking about the first time. Don't feel bad about taking a step back and reaching for one of your favorites. 

What's this say about the work you do? Isn't there something you can do to delight users a second, third time when they come back to whatever it is you make? I think so. Pay attention to the details. The stuff people don't see the first time around. You don't know what's going to grab people. They don't even know what grabs them. So you can't explicitly think of the things that will turn something into a hit. Okay, maybe you can, but that's the kind of thing that's going to compromise your work if you think about it *too much*. Just flesh it out. Be excited about making it, and someone will be excited about using it. Make mistakes, try new things, throw them away, and try more new things, use everything you know, and do it twice. Revise, rewrite, rescore. When you sleep on it, for a month, and it's still awesome to you when you come back to it, it's going to blow everyone away.


