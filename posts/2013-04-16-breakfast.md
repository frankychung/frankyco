---
layout: article
title: Breakfast
slug: breakfast
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: work food
category: article
---

Breakfast is my favorite meal of the day. When you wake up and
you're thirsty and sleepy and hungry, nothing feels better than wiping
all that away with eggs and bacon. That first swig of water. That first
bite into something hearty. That bounce in your step when you're done
and you're washing the dishes. That feeling inside that you stirred
something and you're gearing to go.

It makes the whole day better. I'm not saying you can't have an amazing
day without breakfast, but wouldn't you rather sit down to do your good
work on a non-empty stomach? Wouldn't you rather start your day with a
certain satisfaction instead of a certain sluggishness? Of course.

Everything is like that. How you start anything is how you're going to
do it for the rest of the day. If you sit down and the first thing you
do is open Facebook, well you're going to keep opening Facebook and
reading your feeds and checking your tweets and whatever crazy things we
do these days. And that's totally cool, we need that sometimes, maybe
even a lot of the time, but it's not your best self. 

And if that's the case, then isn't the morning the most important time
of the day? Because that's when you do the first of almost everything.
If you eat a healthy breakfast, doesn't that set the mood for what
you eat? If you tackle something hard right away, doesn't it make it
easier to keep that chat window closed later on? When you take a run
in the morning or a trip to the gym, isn't it easier to turn down that
cheesecake?

Think about the first of everything. Use a good chunk of your willpower
in the beginning. That initial investment, that initial burst of self
discipline, makes everything easier down the road. And you're going to
thank yourself at the end of the day when you feel so good for being
*great*. 

