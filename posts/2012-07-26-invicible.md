---
layout: article
title: Invincible
slug: invincible
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: life
category: article
---
It's worth taking the time to learn how to enjoy spending time alone. It's hard as shit. Don't get me wrong, hanging out with friends is amazing, and probably my favorite thing in the world. It comes natural to you and me. But when you can curl up with a good book, or when you can lose yourself in creating something meaningful by yourself, that's when you become invincible.

I remember being pretty good at it when I was a kid. I read books all the time. I'm not sure how I picked them. I went through all the books in my older brother's bookshelf for sure. And I used to just borrow whatever was recommended at the library since I didn't have the internet to recommend me hundreds of books like I do now.  

At some point I lost it. And I think a lot of it has to do with the internet. I was always connected with instant messaging. And I don't count browsing the internet as enjoyment. You know when you mindlessly browse Facebook for an hour? You feel terrible. It's so different from when you read a good book, or watch a good movie, when you glow afterwards.

I was constantly surrounded by friends and family from high school to the end of college. It's hard not to be in those environments. You play sports, you're in classes, you work in groups, you join clubs, you eat lunch and dinner with friends. You are never alone. And that's amazing. Of course you got the occasional alone time, but that was the exception, not the norm. 

But suddenly, out of nowhere, almost immediately after we threw our caps in the air, I was dropped into reality. I would still see people every day, but there was a sense of aloneness, which is different from loneliness. It wasn't sad. Just foreign, although foreign can be upsetting too. 

So what do I mean by invincible? Well, at all times, you're in one of two states. Alone or not alone. I think most of us are okay with being not alone. But I have a feeling that many of us are uncomfortable being alone, at least for long stretches of time. But if you can learn to take advantage of this time, treasure it, look forward to it, but not force it on yourself, then one hundred percent of the time, you're going to be okay. 

It's not just invincible like a steel bunker. It's invincible like Superman. You can take advantage of your superpower to do good. There are things you can only do alone. Figure out what those things are. Make yourself a better person, or make the world a better place. Once you stop avoiding being alone and start facing it head on, you'll fuel all aspects of your life to new heights, especially when you aren't actually alone. You know not to shy away from others, but not to shy away from being alone either.

Here's something I want to try. I want to hang out with my favorite person in the world, doing my own thing in the same room. Can you imagine that? Just sitting in the same room, maybe even on the same couch, and both of us reading a book. That's special. Isn't that weird? When you figure out how to be truly be alone, you never will be.
