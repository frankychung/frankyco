---
layout: article
title: 1Q84
slug: intrigue-twists-dead-ends
image: ''
embed: ''
link_url: http://www.amazon.com/1Q84-Haruki-Murakami/dp/0307593312
footnotes: ''
tags: murakami 1q84
category: article
---
1Q84 is a fun read. You probably won't be able to put it down for the three or so days it'll take to read such a long novel so thnk about starting it at the beginning of a weekend when you can enjoy staying in. 

It has a great start and moves along at an addicting pace for the first two thirds. The dialogue is, as always, a joy to read and Murakami's descriptions of the everyday and mundane is just the opposite. I don't think I can ever get enough of reading what random, simple recipes his characters are cooking. I think it's because even though they're cooking simple dishes, the way they methodically prepare it conveys they've got it down to a craft, which, for some reason, is enjoyable to watch. There's also the familiar references to music and literature that I'm either not well read or not old enough to know what he's talking about, but it gives the intended effect of accentuating the surreal by grounding it in reality. 

The characters you meet are all, as expected, interesting and, if not identifiable with, at least likeable in their own way. There's the familiar independent, lonely protagonist, although in this case there are two, and an enigmatic female character to move the story along, but the supporting cast is fun too, including a ratty private detective, gay bodyguard, and an eccentric female police officer. There's also the awkward, graphic sex and brutal violence strewn throughout but nothing you can't stomach. 

The third act, unfortunately, brings the book down from its brilliant first two. There are some frustrating plot holes, some tidied up with too convenient of a coincidence, others simply ignored, and the pace is slowed down so much that I actually found myself skimming some passages that felt tiresome. I guess you can only take so much description.  

By the end you're left with more than a few questions. Things are brought up, mysterious, curious things, and are simply left as that. It's disappointing, one because I don't remember so many loose ends not tied up in his other stories, and two because this is such a long novel you'd think explanations would eventually make itself in there.  

Don't get me wrong, not all of it ends in a cloud of smoke. This is a love story with a resolution, a weird, violent love story, but definitely as direct a love story I've read from Murakami. I guess it's a testament to his skill that it never becomes cheesy in that regard.

Overall, I enjoyed it. I don't know how much I can attribute that to the fact I haven't sat down with a long, fulfilling piece of fiction in far too long a time but I can't think of a better way to spend a lazy weekend than being absorbed in an alternate dimension like this.
