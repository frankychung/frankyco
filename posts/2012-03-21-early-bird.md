---
layout: article
title: Early bird
slug: early-bird
image: ''
embed: ''
link_url: ''
footnotes: ''
tags: sleep work
category: article
---
It's hard to wake up early. Well, it's not bad when you're forced to stand at the bus stop for school in the morning. Same with waking up for work. You've got someone to pull you out of bed, whether it's your mom or your paycheck. But what about those days in college when your first class was at two PM? It's the ultimate test of willpower.

Fight the voice in your head telling you to crawl back under the covers. Get up when it's just getting light out. Coffee tastes amazing at dawn. So does a hot breakfast. When you overcome that unbearable sleepiness, and it only takes five minutes and a splash of water to snap out of it, everything is amazing. Food tastes better, books are a joy, and your favorite songs sound even more perfect. (Don't turn on the TV.)

It's because it's dead quiet. Even if there isn't a sound for the rest of the day, the world _feels_ more awake at ten AM. Even if you can't see it, you can sense the people and cars bustling outside. But early in the morning, the world is still asleep. You stole a bit of time for yourself and you can use it however you want.

When you sneak something in before you're supposed to, it's like you have a secret that nobody else knows. It's nothing earth shattering, but not insignificant either, that you've been awake just a little bit longer than everyone else. And it feels like the rest of the day is longer. Say you're used to getting up at nine, but today you managed to get up at six. Normally, by the time it's three, it's mid-afternoon and the day is just about over. But if you were up early, then the same amount of time that passed would have you at noon. The day's hardly started. So use that. Use that to build that thing you've been thinking of building, or reading, or going to see. You can watch a whole movie, or read 5 chapters of a book, or write a few pages, or practice that instrument.

But like I said, it only feels like the day's longer. I'm proposing you sleep the same amount (or more, if you're deprived). Which means the other difficult, maybe even *more* difficult, side of this whole thing is going to sleep earlier. So you have your stash of willpower for the day. Make sure even after waking up early, keeping yourself and your house clean, eating right, exercising, and doing your good work for the day, to save a little bit of that willpower to get yourself in bed early, so all that quiet you owe yourself in the morning is possible.

But what's the point, you ask. Why don't I just do all that stuff at night? I think the key here is, at some point in the day, you need to start winding down. I mean, you could theoretically be wired all the time, but that's a recipe for burnout. So you have those same hours, and you could be doing certain things during those hours, but would you rather be doing those things when you're winding down, or when you're just getting started?
